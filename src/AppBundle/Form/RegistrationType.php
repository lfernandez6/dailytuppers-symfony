<?php

namespace App\AppBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType

{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('username');
        $builder->remove('email');
        $builder->remove('plainPassword');

        $builder->add('name', TextType::class, array(
            'label' => 'Nombre',
            "attr" => array(
                'class' => 'form-control',
                'placeholder' => 'Escribe tu nombre',
            ),
        ));
        $builder->add('surname', TextType::class, array(
            'label' => 'Apellido',
            "attr" => array(
                'class' => 'form-control',
                'placeholder' => 'Escribe tu apellido',
            ),
        ));
        $builder->add('email', EmailType::class, array(
            'label' => 'Email',
            "attr" => array(
                'class' => 'form-control',
                'placeholder' => 'Escribe tu email',
            ),
        ));

        $builder->add('plainPassword', RepeatedType::class, array(
            'type' => PasswordType::class,
            'options' => array(
                'translation_domain' => 'FOSUserBundle',
                'attr' => array(
                    'autocomplete' => 'new-password',
                ),
            ),
            'first_options' => array(
                'label' => 'Contraseña',
                "attr" => array(
                    'class' => 'form-control',
                    'placeholder' => 'Escribe tu contraseña'
                )
            ),
            'second_options' => array(
                'label' => 'Repetir contraseña',
                "attr" => array(
                    'class' => 'form-control',
                    'placeholder' => 'Repite tu contraseña'
                )
            ),
            'invalid_message' => 'fos_user.password.mismatch',
        ));



    }

    public function getParent()

    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()

    {
        return 'app_user_registration';
    }

    public function getName()

    {
        return $this->getBlockPrefix();
    }

}