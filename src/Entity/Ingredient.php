<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IngredientRepository")
 */
class Ingredient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="El nombre del ingrediente no puede estar en blanco")
     */
    private $name;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     * @Assert\NotBlank(message="Debes indicar la cantidad de calorías")
     */
    private $calories;

    /**
     * One Ingredient has One IngredientGroup.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\IngredientGroup")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     * @Assert\NotBlank(message="El grupo no puede estar en blanco")
     */
    private $group;

    /**
     * One Ingredient have Many TuppersIngredients.
     * @ORM\OneToMany(targetEntity="App\Entity\TuppersIngredients", mappedBy="ingredient")
     * @var ArrayCollection
     */
    private $tuppers;

    public function __construct()
    {
        $this->tuppers = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getCalories(): ?string
    {
        return $this->calories;
    }

    public function getGroup(): ? IngredientGroup
    {
        return $this->group;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        $slugify = new Slugify();
        $slug = $slugify->slugify($name);
        $this->setSlug($slug);

        return $this;
    }

    public function setCalories(int $calories): self
    {
        $this->calories = $calories;

        return $this;
    }

    public function setGroup(IngredientGroup $group): self
    {
        $this->group = $group;

        return $this;
    }

    public function getTuppers(): Collection
    {
        return $this->tuppers;
    }

    public function addTupper(TuppersIngredients $tupperIngredient): self
    {
        if(!$this->tuppers->contains($tupperIngredient)){
            $this->tuppers->add($tupperIngredient);
            $tupperIngredient->setIngredient($this); // synchronously updating inverse side
        }

        return $this;
    }

    public function removeTupper(TuppersIngredients $tupperIngredient): self
    {
        if ($this->tuppers->contains($tupperIngredient)) {
            $this->tuppers->removeElement($tupperIngredient);
            $tupperIngredient->getTupper()->removeIngredient($tupperIngredient); // synchronously updating inverse side
        }

        return $this;
    }
}
