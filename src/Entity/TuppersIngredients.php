<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TupperIngredientRepository")
 */
class TuppersIngredients
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tupper", inversedBy="ingredients")
     * @ORM\JoinColumn(name="tupper_id", referencedColumnName="id")
     */
    private $tupper;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ingredient", inversedBy="tuppers")
     * @ORM\JoinColumn(name="ingredient_id", referencedColumnName="id")
     */
    private $ingredient;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTupper(): ?Tupper
    {
        return $this->tupper;
    }

    public function getIngredient(): ?Ingredient
    {
        return $this->ingredient;
    }

    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    public function setTupper(Tupper $tupper): self
    {
        $this->tupper = $tupper;

        return $this;
    }

    public function setIngredient(Ingredient $ingredient): self
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    public function setQuantity(string $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
