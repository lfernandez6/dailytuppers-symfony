<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 * @UniqueEntity("slug")
 */
class Tag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * Many Tags have Many Tuppers.
     * @ORM\ManyToMany(targetEntity="App\Entity\Tupper", inversedBy="tags")
     * @ORM\JoinTable(name="tuppers_tags")
     * @var ArrayCollection
     */
    private $tuppers;

    public function __construct()
    {
        $this->tuppers = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->slug;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getTuppers(): Collection
    {
        return $this->tuppers;
    }

    public function addTupper(Tupper $tupper): self
    {
        if (!$this->tuppers->contains($tupper)) {
            $this->tuppers->add($tupper);
        }

        return $this;
    }

    public function removeTupper(Tupper $tupper): self
    {
        if ($this->tuppers->contains($tupper)) {
            $this->tuppers->removeElement($tupper);
        }

        return $this;
    }
}
