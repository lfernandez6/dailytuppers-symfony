<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use DateTimeInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 3,
     *      max = 20,
     *      minMessage = "Tu nombre debe tener almenos {{ limit }} caracteres",
     *      maxMessage = "Tu nombre no puede tener más de {{ limit }} caracteres"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = 3,
     *      max = 50,
     *      minMessage = "Tu apellido debe tener almenos {{ limit }} caracteres",
     *      maxMessage = "Tu apellido no puede tener más de {{ limit }} caracteres"
     * )
     */
    private $surname;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $registerDate;

    /**
     * @ORM\Column(type="boolean", options={"default": "0"})
     */
    private $banned;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tupper", mappedBy="user", cascade={"remove"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    private $tuppers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Rating", mappedBy="user", cascade={"remove"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    private $ratings;

    public function __construct()
    {
        parent::__construct();
        $this->tuppers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function getRegisterDate(): ?DateTimeInterface
    {
        return $this->registerDate;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setSurname(?string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function setEmail($email){
        parent::setEmail($email);
        parent::setUsername($email);
    }

    public function setRegisterDate(DateTimeInterface $registerDate): self
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    public function setBanned($banned) : self
    {
        $this->banned = (bool) $banned;

        return $this;
    }

    public function isBanned()
    {
        return $this->banned;
    }

    public function getTuppers(): Collection
    {
        return $this->tuppers;
    }

    public function addTupper(Tupper $tupper): self
    {
        $this->tuppers[] = $tupper;

        return $this;
    }

    public function removeTupper(Tupper $tupper): self
    {
        if ($this->tuppers->contains($tupper)) {
            $this->tuppers->removeElement($tupper);
        }

        return $this;
    }

    public function getRatings(): Collection
    {
        return $this->ratings;
    }

    public function addRating(Rating $rating): self
    {
        $this->ratings[] = $rating;

        return $this;
    }

    public function removeRating(Rating $rating): self
    {
        if ($this->ratings->contains($rating)) {
            $this->ratings->removeElement($rating);
        }

        return $this;
    }
}
