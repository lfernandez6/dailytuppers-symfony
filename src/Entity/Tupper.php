<?php

namespace App\Entity;

use App\Utils\Difficulty;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use DateTimeInterface;
use Cocur\Slugify\Slugify;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Events;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TupperRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Tupper
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min = 3,
     *      max = 100,
     *      minMessage = "El título de tu tupper debe tener almenos {{ limit }} caracteres",
     *      maxMessage = "El título de tu tupper no puede tener más de {{ limit }} caracteres"
     * )
     */
    private $title;

    /**
     * One Tupper has One Category.
     * @ORM\ManyToOne(targetEntity="App\Entity\Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\Column(type="integer", options={"comment":"low=1, medium=2, hight=3"})
     */
    private $difficulty;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $duration;

    /**
     * @ORM\Column(type="integer", options={"default" : 1})
     * @Assert\GreaterThan(
     *     value = 0,
     *     message = "El número de comensales debe ser mayor que {{ value }}"
     * )
     */
    private $commensals;

    /**
     * @ORM\Column(type="text")
     */
    private $steps;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\File(
     *     maxSize = "1500k",
     *     maxSizeMessage = "La imagen es muy grande. Sube una imagen con tamaño máximo de 1.5M",
     *     mimeTypes={ "image/jpeg", "image/png" },
     *     mimeTypesMessage = "Por favor, sube una imagen válida (png/jpg)"
     * )
     */
    private $image;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $creationDate;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default": NULL})
     */
    private $updateDate;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $published = true;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $visits = 0;

    /**
     * One Tupper has One User.
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tuppers")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Many Tuppers have Many Tags.
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", mappedBy="tuppers", cascade={"persist"})
     */
    private $tags;

    /**
     * One Tupper have Many TuppersIngredients.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TuppersIngredients", mappedBy="tupper", cascade={"persist", "remove"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    private $ingredients;

    /**
     * One Tupper have Many Rating.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Rating", mappedBy="tupper", cascade={"persist", "remove"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    private $ratings;

    /**
     * @var int
     */
    private $totalRate;

    private $overrideImage = false;

    public function __construct()
    {
        $this->ingredients = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->ratings = new ArrayCollection();
    }

    /**
     * @ORM\PostRemove
     */
    public function deleteImagePostRemove()
    {
        $container = $GLOBALS['kernel']->getContainer();
        $path = $container->getParameter('images_directory');
        $file = $this->getImage();
        $image = "{$path}/{$file}";
        if (file_exists($image)) {
            unlink($image);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function getDifficulty(): ?int
    {
        return $this->difficulty;
    }

    public function getDifficultyDescrition(): string
    {
        return Difficulty::getDescription($this->difficulty);
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function getCommensals(): ?int
    {
        return $this->commensals;
    }

    public function getSteps(): ?string
    {
        return $this->steps;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getCreationDate(): ?DateTimeInterface
    {
        return $this->creationDate;
    }

    public function getUpdateDate(): ?DateTimeInterface
    {
        return $this->updateDate;
    }

    public function isPublished(): ?bool
    {
        return $this->published;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getVisits(): ?int
    {
        return $this->visits;
    }

    public function getTotalRate(): ?int {
        $avg = 0;
        if(count($this->ratings) > 0){
            $total = 0;
            $numRates = count($this->ratings);

            /** @var $rating Rating */
            foreach($this->ratings as $rating){
                $total = $total + $rating->getRate();
            }

            $avg = round($total/$numRates, 0);
        }

        $this->setTotalRate($avg);

        return $this->totalRate;
    }

    public function getOverrideImage(): bool
    {
        return $this->overrideImage;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        $slugify = new Slugify();
        $slug = $slugify->slugify($title);
        $this->setSlug($slug);

        return $this;
    }

    public function setCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function setDifficulty(int $difficulty): self
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    public function setDuration(string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function setCommensals(int $commensals): self
    {
        $this->commensals = $commensals;

        return $this;
    }

    public function setSteps(string $steps): self
    {
        $this->steps = $steps;

        return $this;
    }

    public function setImage($image): self
    {
        $this->image = $image;

        return $this;
    }

    public function setCreationDate(DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function setUpdateDate(DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function setVisits(int $visits): self
    {
        $this->visits = $visits;

        return $this;
    }

    public function setTotalRate(int $totalRate): self
    {
        $this->totalRate = $totalRate;

        return $this;
    }

    public function setOverrideImage(bool $overrideImage): self
    {
        $this->overrideImage = $overrideImage;

        return $this;
    }

    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function findTag(string $slug)
    {
        foreach($this->getTags() as $tag){
            if($tag->getSlug() === $slug){
                return true;
            }
        }
        return false;
    }

    public function addTag(Tag $tag): self
    {
        //if tag is already on the tupper, don't save it again
        if($this->findTag($tag->getSlug())){
            return $this;
        }

        if (!$this->tags->contains($tag)) {
            $tag->addTupper($this); // synchronously updating inverse side
            $this->tags->add($tag);
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            $tag->removeTupper($this); // synchronously updating inverse side
        }


        return $this;
    }

    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    public function addIngredient(TuppersIngredients $tupperIngredient): self
    {
        if (!$this->ingredients->contains($tupperIngredient)) {
            $tupperIngredient->setTupper($this); // synchronously updating inverse side
            $this->ingredients->add($tupperIngredient);
        }

        return $this;
    }

    public function removeIngredient(TuppersIngredients $tupperIngredient): self
    {
        if ($this->ingredients->contains($tupperIngredient)) {
            $this->ingredients->removeElement($tupperIngredient);
            $tupperIngredient->getIngredient()->removeTupper($tupperIngredient); // synchronously updating inverse side
        }

        return $this;
    }

    public function getRatings(): Collection
    {
        return $this->ratings;
    }

    public function addRating(Rating $rating): self
    {
        $this->ratings[] = $rating;
        $rating->setTupper($this);

        return $this;
    }

    public function removeRating(Rating $rating): self
    {
        if ($this->ratings->contains($rating)) {
            $this->ratings->removeElement($rating);
            $rating->getTupper()->removeRating($rating); // synchronously updating inverse side
        }

        return $this;
    }

    /**
     * Returns tupper detail url
     *
     * @return string
     */
    public function getUrl() : string
    {
        return '/tupper/' . $this->getSlug();
    }
}
