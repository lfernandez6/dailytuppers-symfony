<?php

namespace App\Repository;

use App\Entity\Rating;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Rating|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rating|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rating[]    findAll()
 * @method Rating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RatingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Rating::class);
    }

    // /**
    //  * @return Rating[] Returns an array of Rating objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Rating
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */




    /*
     * SELECT tupper_id, AVG(rate) as avgrate
FROM db_name.rating
group by tupper_id
order by avgrate desc;
     */
    public function findMostRated(int $numTuppers = 20, $userId = null)
    {
        $qb = $this->createQueryBuilder('r')
            ->select([
                'r',
                'avg(r.rate) as avgrate'
            ])
            ->leftJoin('r.tupper', 't')
            ->groupBy('r.tupper')
            ->orderBy('avgrate', 'desc')
            ->setMaxResults($numTuppers)
            ->getQuery()
            ->getResult()
            ;

        return $qb;


//        $qb = $this->createQueryBuilder('r')
//            ->leftJoin('r.tupper', 't')
//            ->leftJoin('t.tags', 'a')
//            ->addSelect([
//                'avg(r.rate) as avgrate'
//            ]);
//
//        if (null !== $userId) {
//            $qb
//                ->andWhere('t.user = :id')
//                ->setParameter('id', $userId);
//        }
//
//        $qb
//            ->andWhere('t.published = :published')
//            ->setParameter('published', true)
//            ->groupBy('r.tupper')
//            //->orderBy('avgrate', 'desc')
//            ->setMaxResults($numTuppers)
//            ->getQuery()
//            ->getResult()
//            ;
    }
}
