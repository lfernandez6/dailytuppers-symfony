<?php

namespace App\Repository;

use App\Entity\Category;
use App\Utils\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    protected $paginator;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
        $this->paginator = new Paginator();
    }

    public function findOneBySlug($value): ?Category
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.slug= :slug')
            ->setParameter('slug', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findTopCategories($limit = 6)
    {
        $qb = $this->createQueryBuilder('c');

        return $qb
            ->select([
                'id',
                'slug',
                'name',
                'count(c.id) as ntuppers'
            ])
            ->innerJoin('tupper', 't')
            ->groupBy('c.id')
            ->orderBy('ntuppers', 'desc')
            ->getQuery()
            ->getResult();
    }

    public function getAllCategories($currentPage = 1, int $limit = 20)
    {
        // Create our query
        $query = $this->createQueryBuilder('c');

        $query
            ->orderBy('c.name', 'asc')
            ->getQuery();

        $paginator = $this->paginator->paginate($query, $currentPage, $limit);

        return $paginator;
    }

    public function getTotalTupperByCategory()
    {
        $sql = "SELECT c.name, temp.total 
        FROM category c
        LEFT JOIN (
          SELECT category_id , count(*) AS total
          FROM tupper t
          GROUP BY t.category_id
        ) temp ON temp.category_id = c.id
        ORDER BY temp.total DESC";

        $em = $this->_em;
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();

    }
}
