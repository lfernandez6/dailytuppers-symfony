<?php

namespace App\Repository;

use App\Entity\User;
use App\Utils\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use InvalidArgumentException;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    protected $paginator;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
        $this->paginator = new Paginator();
    }

//    public function findWithTuppers($id): ?User
//    {
//        if (empty($id)) {
//            throw new InvalidArgumentException('The id should not be empty.');
//        }
//
//        return $this->createQueryBuilder('u')
//            ->join('u.tuppers', 't')
//            ->andWhere('u.id = :id')
//            ->orderBy('t.creationDate', 'desc')
//            ->setParameter('id', $id)
//            ->getQuery()
//            ->getResults()
//            ;
//    }

    /**
     * @param int $currentPage
     * @param int $limit
     * @param $search
     * @return \Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function getAllUsers($currentPage = 1, int $limit = 20, $search = null)
    {
        // Create our query
        $query = $this->createQueryBuilder('u');

        if(null !== $search){
            $query
                ->andWhere('u.email LIKE :email')
                ->setParameter('email', "%$search%");
        }

        $query
            ->andWhere('u.roles NOT LIKE :roles')
            ->setParameter('roles', '%"ROLE_SUPER_ADMIN"%')
            ->orderBy('u.registerDate', 'desc')
            ->getQuery();

        $paginator = $this->paginator->paginate($query, $currentPage, $limit);

        return $paginator;
    }

    public function getUsersStatus()
    {
        return $this->createQueryBuilder('u')
            ->select([
                'count(u.id) as total',
                'u.banned',
                'u.enabled'
            ])
            ->groupBy('u.banned, u.enabled')
            ->getQuery()
            ->getResult();
    }

    public function parseStatusResults($data)
    {
        $results = [];
        $total = 0;

        foreach ($data as $key=>$value){
            if(false === $value['banned'] && false === $value['enabled']){
                $results['pending']['title'] = "Pendientes";
                $results['pending']['value'] = (int) $value['total'];
                $total += (int) $value['total'];
            }elseif(false === $value['banned'] && true === $value['enabled']){
                $results['active']['title'] = "Activos";
                $results['active']['value'] = (int) $value['total'];
                $total += (int) $value['total'];
            }elseif(true === $value['banned']){
                $results['banned']['title'] = "Bloqueados";
                $results['banned']['value'] = (int) $value['total'];
                $total += (int) $value['total'];
            }
        }
        $results['total']['title'] = "Total";
        $results['total']['value'] = $total;

        return $results;
    }
}
