<?php

namespace App\Repository;

use App\Entity\Ingredient;
use App\Utils\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Ingredient|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ingredient|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ingredient[]    findAll()
 * @method Ingredient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IngredientRepository extends ServiceEntityRepository
{
    protected $paginator;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ingredient::class);
        $this->paginator = new Paginator();
    }

    public function findLike(string $name): ?array
    {
        return $this
            ->createQueryBuilder('a')
            ->where('a.name LIKE :name')
            ->setParameter('name', "%$name%")
            ->orderBy('a.name')
            ->setMaxResults(50)
            ->orderBy('LENGTH(a.name)', 'ASC')
            ->getQuery()
            ->execute()
            ;
    }

    /**
     * @param int $currentPage
     * @param int $limit
     * @param $group
     * @param null $search
     * @return \Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function getAllIngredients($currentPage = 1, int $limit = 20, $group, $search = null)
    {
        // Create our query
        $query = $this->createQueryBuilder('i');

        if(null !== $search){
            $query
                ->andWhere('i.name LIKE :name')
                ->setParameter('name', "%$search%");
        }

        $query
            ->andWhere('i.group = :group')
            ->setParameter(':group', $group)
            ->orderBy('i.group, i.name', 'asc')
            ->getQuery();

        $paginator = $this->paginator->paginate($query, $currentPage, $limit);

        return $paginator;
    }


}
