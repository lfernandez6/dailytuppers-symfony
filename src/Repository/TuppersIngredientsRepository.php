<?php

namespace App\Repository;

use App\Entity\TuppersIngredients;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TuppersIngredients|null find($id, $lockMode = null, $lockVersion = null)
 * @method TuppersIngredients|null findOneBy(array $criteria, array $orderBy = null)
 * @method TuppersIngredients[]    findAll()
 * @method TuppersIngredients[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TuppersIngredientsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TuppersIngredients::class);
    }

    // /**
    //  * @return TupperIngredient[] Returns an array of TupperIngredient objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TupperIngredient
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
