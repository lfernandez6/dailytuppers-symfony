<?php

namespace App\Repository;

use App\Entity\Tupper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Tupper|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tupper|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tupper[]    findAll()
 * @method Tupper[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TupperRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Tupper::class);
    }


    /**
     * Paginator Helper
     *
     * Pass through a query object, current page & limit
     * the offset is calculated from the page and limit
     * returns an `Paginator` instance, which you can call the following on:
     *
     *     $paginator->getIterator()->count() # Total fetched (ie: `5` posts)
     *     $paginator->count() # Count of ALL posts (ie: `20` posts)
     *     $paginator->getIterator() # ArrayIterator
     *
     * @param Query $dql         DQL Query Object
     * @param integer            $page  Current page (defaults to 1)
     * @param integer            $limit The total number per page (defaults to 3)
     *
     * @return Paginator
     */
    public function paginate($dql, $page = 1, $limit = 3)
    {
        $page = (null === $page) ? 1 : $page;

        $paginator = new Paginator($dql);

        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1)) // Offset
            ->setMaxResults($limit); // Limit

        return $paginator;
    }

    /**
     * Return all tuppers ordered by creation-date (desc)
     * findLast() method
     *
     * 1. Create & pass query to paginate method
     * 2. Paginate will return a `\Doctrine\ORM\Tools\Pagination\Paginator` object
     * 3. Return that object to the controller
     *
     * @param int $currentPage
     * @param int $limit
     * @param null $userId
     * @param null $search
     * @param null $category
     * @param null $difficulty
     * @param null $duration
     * @param true $published
     *
     * @return Paginator
     */
    public function findLast($currentPage = 1, int $limit = 20, $userId = null, $search = null, $category = null, $difficulty = null, $duration = null, $published = true)
    {
        // Create our query
        $query = $this->createQueryBuilder('t');

        if (null !== $userId) {
            $query
                ->andWhere('t.user = :id')
                ->setParameter('id', $userId);
        }

        if(null !== $search){
            $query
                ->andWhere('t.title LIKE :name')
                ->setParameter('name', "%$search%");
        }
        if(null !== $category){
            $query
                ->andWhere('t.category = :category')
                ->setParameter('category', $category);
        }
        if(null !== $difficulty){
            $query
                ->andWhere('t.difficulty = :difficulty')
                ->setParameter('difficulty', $difficulty);
        }
        if(null !== $duration){
            $query
                ->andWhere('t.duration = :duration')
                ->setParameter('duration', $duration);
        }
        if(null !== $published){
            $query
                ->andWhere('t.published = :published')
                ->setParameter('published', $published);
        }

        $query
            ->orderBy('t.creationDate', 'desc')
            ->getQuery();

        // No need to manually get get the result ($query->getResult())

        $paginator = $this->paginate($query, $currentPage, $limit);

        return $paginator;
    }


    /**
     * Return all tuppers ordered by total-visits (desc)
     *
     * @param int $currentPage
     * @param int $limit
     * @param null $userId
     * @param null $search
     * @param null $category
     * @param null $difficulty
     * @param null $duration
     * @param true $published
     *
     * @return Paginator
     */
    public function findMostVisited($currentPage = 1, int $limit = 20, $userId = null, $search = null, $category = null, $difficulty = null, $duration = null, $published = true)
    {
        $query = $this->createQueryBuilder('t');

        if (null !== $userId) {
            $query
                ->andWhere('t.user = :id')
                ->setParameter('id', $userId);
        }

        if(null !== $search){
            $query
                ->andWhere('t.title LIKE :name')
                ->setParameter('name', "%$search%");
        }
        if(null !== $category){
            $query
                ->andWhere('t.category = :category')
                ->setParameter('category', $category);
        }
        if(null !== $difficulty){
            $query
                ->andWhere('t.difficulty = :difficulty')
                ->setParameter('difficulty', $difficulty);
        }
        if(null !== $duration){
            $query
                ->andWhere('t.duration = :duration')
                ->setParameter('duration', $duration);
        }
        if(null !== $published){
            $query
                ->andWhere('t.published = :published')
                ->setParameter('published', $published);
        }

        $query
            ->orderBy('t.visits', 'desc')
            ->getQuery();

        $paginator = $this->paginate($query, $currentPage, $limit);

        return $paginator;
    }

    /**
     * Return all tuppers ordered by avg-rate (desc)
     *
     * @param int $currentPage
     * @param int $limit
     * @param null $userId
     * @param null $search
     * @param null $category
     * @param null $difficulty
     * @param null $duration
     * @param true $published
     *
     * @return Paginator
     */
    public function findMostRated($currentPage = 1, int $limit = 20, $userId = null, $search = null, $category = null, $difficulty = null, $duration = null, $published = true)
    {
        $query = $this->createQueryBuilder('t');

        if (null !== $userId) {
            $query
                ->andWhere('t.user = :id')
                ->setParameter('id', $userId);
        }

        $query
            ->select([
                't',
                'avg(r.rate) as avgrate'
            ])
            ->leftJoin('t.ratings', 'r');

        if(null !== $search){
            $query
                ->andWhere('t.title LIKE :name')
                ->setParameter('name', "%$search%");
        }
        if(null !== $category){
            $query
                ->andWhere('t.category = :category')
                ->setParameter('category', $category);
        }
        if(null !== $difficulty){
            $query
                ->andWhere('t.difficulty = :difficulty')
                ->setParameter('difficulty', $difficulty);
        }
        if(null !== $duration){
            $query
                ->andWhere('t.duration = :duration')
                ->setParameter('duration', $duration);
        }
        if(null !== $published){
            $query
                ->andWhere('t.published = :published')
                ->setParameter('published', $published);
        }

        $query
            ->groupBy('r.tupper')
            ->orderBy('avgrate', 'desc')
            ->getQuery();

        $paginator = $this->paginate($query, $currentPage, $limit);

        return $paginator;
    }

    public function parseResults($data)
    {
        $results = [];

        if(empty($data)){
            return $results;
        }

        foreach ($data as $item){
            $results[] = $item[0];
        }
        return$results;
    }

    /**
     * Return the categories more used (with more tuppers associated)
     *
     * @param int $limit
     * @return mixed
     */
    public function findTopCategories($limit = 6){
        $query = $this->createQueryBuilder('t');

        return $query
            ->select([
                'c.id',
                'c.slug',
                'c.name',
                'count(t.id) as ntuppers'
            ])
            ->innerJoin('t.category', 'c')
            ->groupBy('t.category')
            ->orderBy('ntuppers', 'desc')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * Returns the tuppers assigned to an $slug CATEGORY
     *
     * @param $slug
     * @param int $currentPage
     * @param int $limit
     * @param null $sort
     * @param null $search
     * @param null $category
     * @param null $difficulty
     * @param null $duration
     * @param true $published
     *
     * @return array|Paginator
     */
    public function findByCategory($slug, $currentPage = 1, $limit = 20, $sort = null, $search = null, $category = null, $difficulty = null, $duration = null, $published = true)
    {
        if("" == $slug){
            return [];
        }

        $query = $this->createQueryBuilder('t');

        if('valorados' == $sort){
            $query
                ->select([
                    't',
                    'avg(r.rate) as avgrate'
                ])
                ->leftJoin('t.ratings', 'r');
        }

        $query
            ->innerJoin('t.category', 'c')
            ->andWhere('c.slug = :slug')
            ->setParameter('slug', $slug);

        if(null !== $search){
            $query
                ->andWhere('t.title LIKE :name')
                ->setParameter('name', "%$search%");
        }
        if(null !== $category){
            $query
                ->andWhere('t.category = :category')
                ->setParameter('category', $category);
        }
        if(null !== $difficulty){
            $query
                ->andWhere('t.difficulty = :difficulty')
                ->setParameter('difficulty', $difficulty);
        }
        if(null !== $duration){
            $query
                ->andWhere('t.duration = :duration')
                ->setParameter('duration', $duration);
        }
        if(null !== $published){
            $query
                ->andWhere('t.published = :published')
                ->setParameter('published', $published);
        }

        switch ($sort){
            case 'visitados':
                $query->orderBy('t.visits', 'desc');
                break;

            case 'valorados':
                $query
                    ->groupBy('r.tupper')
                    ->orderBy('avgrate', 'desc');
                break;

            case 'recientes':
            default:
                $query->orderBy('t.creationDate', 'desc');
                break;
        }


        $query
            ->getQuery();

        $paginator = $this->paginate($query, $currentPage, $limit);

        return $paginator;
    }

    /**
     * Returns the tuppers assigned to an $slug TAG
     *
     * @param $slug
     * @param int $currentPage
     * @param int $limit
     * @param null $sort
     * @param null $search
     * @param null $category
     * @param null $difficulty
     * @param null $duration
     * @param true $published
     *
     * @return array|mixed
     */
    public function findByTag($slug, $currentPage = 1, $limit = 20, $sort = null, $search = null, $category = null, $difficulty = null, $duration = null, $published = true)
    {
        if("" == $slug){
            return [];
        }

        $query = $this->createQueryBuilder('t');

        if('valorados' == $sort){
            $query
                ->select([
                    't',
                    'avg(r.rate) as avgrate'
                ])
                ->leftJoin('t.ratings', 'r');
        }

        $query
            ->innerJoin('t.tags', 'ta')
            ->andWhere('ta.slug = :slug')
            ->setParameter('slug', $slug);

        if(null !== $search){
            $query
                ->andWhere('t.title LIKE :name')
                ->setParameter('name', "%$search%");
        }
        if(null !== $category){
            $query
                ->andWhere('t.category = :category')
                ->setParameter('category', $category);
        }
        if(null !== $difficulty){
            $query
                ->andWhere('t.difficulty = :difficulty')
                ->setParameter('difficulty', $difficulty);
        }
        if(null !== $duration){
            $query
                ->andWhere('t.duration = :duration')
                ->setParameter('duration', $duration);
        }
        if(null !== $published){
            $query
                ->andWhere('t.published = :published')
                ->setParameter('published', $published);
        }

        switch ($sort){
            case 'visitados':
                $query->orderBy('t.visits', 'desc');
                break;

            case 'valorados':
                $query
                    ->groupBy('r.tupper')
                    ->orderBy('avgrate', 'desc');
                break;

            case 'recientes':
            default:
                $query->orderBy('t.creationDate', 'desc');
                break;
        }

        $query
            ->getQuery();

        $paginator = $this->paginate($query, $currentPage, $limit);

        return $paginator;

    }

    /**
     * Returns the tuppers assigned to an $slug TAG
     *
     * @param $ingredients
     * @param int $currentPage
     * @param int $limit
     * @param null $sort
     * @param null $search
     * @param null $category
     * @param null $difficulty
     * @param null $duration
     * @param true $published
     *
     * @return array|mixed
     */
    public function findByIngredient(array $ingredients, $currentPage = 1, $limit = 20, $sort = null, $search = null, $category = null, $difficulty = null, $duration = null, $published = true)
    {

        $query = $this->createQueryBuilder('t');

        if('valorados' == $sort){
            $query
                ->select([
                    't',
                    'avg(r.rate) as avgrate'
                ])
                ->leftJoin('t.ratings', 'r');
        }

        $query
            ->innerJoin('t.ingredients', 'tup_ing')
            ->innerJoin('tup_ing.ingredient', 'ing')
            ->andWhere('ing.id IN (:array_ing)')
            ->setParameter('array_ing', $ingredients);

        if(null !== $search){
            $query
                ->andWhere('t.title LIKE :name')
                ->setParameter('name', "%$search%");
        }
        if(null !== $category){
            $query
                ->andWhere('t.category = :category')
                ->setParameter('category', $category);
        }
        if(null !== $difficulty){
            $query
                ->andWhere('t.difficulty = :difficulty')
                ->setParameter('difficulty', $difficulty);
        }
        if(null !== $duration){
            $query
                ->andWhere('t.duration = :duration')
                ->setParameter('duration', $duration);
        }
        if(null !== $published){
            $query
                ->andWhere('t.published = :published')
                ->setParameter('published', $published);
        }

        switch ($sort){
            case 'visitados':
                $query->orderBy('t.visits', 'desc');
                break;

            case 'valorados':
                $query
                    ->groupBy('r.tupper')
                    ->orderBy('avgrate', 'desc');
                break;

            case 'recientes':
            default:
                $query->orderBy('t.creationDate', 'desc');
                break;
        }

        $query
            ->getQuery();

        $paginator = $this->paginate($query, $currentPage, $limit);

        return $paginator;

    }

    public function getTuppersStatus()
    {
        return $this->createQueryBuilder('t')
            ->select([
                'count(t.id) as total',
                't.published'
            ])
            ->groupBy('t.published')
            ->getQuery()
            ->getResult();
    }

    public function parteStatusResults($data)
    {
        $results = [];
        $total = 0;
        foreach ($data as $value){
            if(true === $value['published']){
                $results ['published'] = [
                    'title' => "Publicados",
                    'value' => (int) $value['total'],
                ];
                $total += (int) $value['total'];
            }else{
                $results ['unpublished'] = [
                    'title' => "Despublicados",
                    'value' => (int) $value['total'],
                ];
                $total += (int) $value['total'];
            }
        }
        $results ['total'] = [
            'title' => "Total",
            'value' => $total,
        ];

        return $results;
    }
}
