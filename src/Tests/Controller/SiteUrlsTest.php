<?php


namespace App\Tests\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\BrowserKit\Cookie;

class SiteUrlsTest extends WebTestCase
{
    /**
     * Test NOT logged Urls
     *
     * @dataProvider provideNotLoggedUrls
     */
    public function testNotLoggedPageIsSuccessful($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    /**
     * Test LOGGED Urls
     *
     * @dataProvider provideLoggedUrls
     */
    public function testLoggedPageIsSuccessful($url)
    {
        $client = $this->createAuthorizedClient();

        $crawler = $client->request('GET', $url);
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );

    }

    /**
     * Test ADMIN Urls
     *
     * @dataProvider provideAdminUrls
     */
    public function testAdminPageIsSuccessful($url)
    {
        $client = $this->createAuthorizedAdminClient();

        $crawler = $client->request('GET', $url);
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );

    }

    /**
     * Test ADMIN Urls with Default User
     *
     * @dataProvider provideAdminUrls
     */
    public function testAdminPageForbidden($url)
    {
        $client = $this->createAuthorizedClient();

        $crawler = $client->request('GET', $url);
        $this->assertEquals(
            403,
            $client->getResponse()->getStatusCode()
        );

    }

    /**
     * Test Logged Urls with NOT logged user
     *
     * @dataProvider provideLoggedUrls
     * @param $url
     */
    public function testLoggedPageForbidden($url)
    {
        $client = self::createClient();

        $crawler = $client->request('GET', $url);
        $this->assertEquals(
            302,
            $client->getResponse()->getStatusCode()
        );

    }

    /**
     * Accessible urls if user NOT logged     *
     * @return array
     */
    public function provideNotLoggedUrls()
    {
        return [
            ['/'],
            ['/iniciar-sesion'],
            ['/registro'],
            ['/recuperar-contrasena'],
            ['/explorar-tuppers'],
            ['/explorar-tuppers/mejor-valorados'],
            ['/explorar-tuppers/mas-visitados'],
            ['/explorar-tuppers/categoria/pastas'],
            ['/explorar-tuppers/tag/test'],
            ['/busqueda-por-ingrediente'],
            ['/tupper/hola'],
        ];
    }

    /**
     * Accessible urls if user logged
     * @return array
     */
    public function provideLoggedUrls()
    {
        return [
            ['/mi-cuenta'],
            ['/mi-cuenta/mis-tuppers/mas-visitados'],
            ['/mi-cuenta/mis-tuppers/mejor-valorados'],
            ['/mi-cuenta/mis-tuppers'],
            ['/mi-cuenta/crear-tupper'],
            ['/mi-cuenta/editar-tupper/46'],
        ];
    }

    /**
     * Accessible urls if user ADMIN
     * @return array
     */
    public function provideAdminUrls()
    {
        return [
            ['/admin'],
            ['/admin/usuarios'],
            ['/admin/tuppers'],
            ['/admin/ingredientes'],
            ['/admin/ingredientes/crear'],
            ['/admin/categorias'],
            ['/admin/categorias/crear'],
        ];
    }

    /**
     * Log a normal user
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    protected function createAuthorizedClient()
    {
        $client = static::createClient();
        $container = static::$kernel->getContainer();
        $session = $container->get('session');
        $user = self::$kernel->getContainer()->get('doctrine')->getRepository(User::class)->findOneByEmail('lfernandez6+test2@gmail.com');

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();

        $client->getCookieJar()->set(new Cookie($session->getName(), $session->getId()));

        return $client;
    }

    /**
     * Log ADMIN user
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    protected function createAuthorizedAdminClient()
    {
        $client = static::createClient();
        $container = static::$kernel->getContainer();
        $session = $container->get('session');
        $user = self::$kernel->getContainer()->get('doctrine')->getRepository(User::class)->findOneByEmail('lfernandez6+admin@gmail.com');

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();

        $client->getCookieJar()->set(new Cookie($session->getName(), $session->getId()));

        return $client;
    }

}