<?php


namespace App\Tests\Repository;


use App\Entity\Category;
use App\Entity\Tupper;
use App\Repository\CategoryRepository;
use App\Repository\TupperRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testSearchCategories()
    {
        /** @var CategoryRepository $repository */
        $repository = self::$kernel->getContainer()->get('doctrine')->getRepository(Category::class);
        $categories = $repository->getAllCategories();

        $this->assertNotEmpty($categories);
    }

    public function testGetLastTuppers()
    {
        /** @var TupperRepository $repository */
        $repository = self::$kernel->getContainer()->get('doctrine')->getRepository(Tupper::class);
        $tuppers = $repository->findLast();

        $this->assertNotEmpty($tuppers);
    }

    public function testGetRatedTuppers()
    {
        /** @var TupperRepository $repository */
        $repository = self::$kernel->getContainer()->get('doctrine')->getRepository(Tupper::class);
        $tuppers = $repository->findMostRated();

        $this->assertNotEmpty($tuppers);
    }

    public function testGetVisitedTuppers()
    {
        /** @var TupperRepository $repository */
        $repository = self::$kernel->getContainer()->get('doctrine')->getRepository(Tupper::class);
        $tuppers = $repository->findMostVisited();

        $this->assertNotEmpty($tuppers);
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}