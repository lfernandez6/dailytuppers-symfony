<?php


namespace App\Tests\Util;

use App\Utils\Difficulty;
use App\Utils\Duration;
use PHPUnit\Framework\TestCase;

class GeneralTest extends TestCase
{
    public function testGetDifficulties()
    {
        $difficulties = Difficulty::getValues();

        $this->assertNotEmpty($difficulties);
    }

    public function testGetDurations()
    {
        $durations = Duration::getValues();

        $this->assertNotEmpty($durations);
    }
}