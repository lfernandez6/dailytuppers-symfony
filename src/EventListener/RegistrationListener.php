<?php
namespace App\EventListener;

use App\Entity\User;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Listener responsible for adding the default user role at registration
 */
class RegistrationListener implements EventSubscriberInterface
{
    protected $router;
    protected $security;
    public function __construct(Router $router, AuthorizationChecker $security)
    {
        $this->router = $router;
        $this->security = $security;
    }


    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegistrationInitialize',
        );
    }

    public function onRegistrationSuccess(FormEvent $event)
    {
        /** @var User $user */
        $user = $event->getForm()->getData();
        $user->setRoles(['ROLE_DEFAULT_USER']);
        $user->setRegisterDate(new DateTime());
        $user->setBanned(false);
    }

    public function onRegistrationInitialize(GetResponseUserEvent $event)
    {
        // If user logged, redirecto to /mi-cuenta
        if($this->security->isGranted('IS_AUTHENTICATED_FULLY')){
            $url = $this->router->generate('account');
            $response = new RedirectResponse($url);

            $event->setResponse($response);
        }
    }
}