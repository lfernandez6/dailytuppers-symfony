<?php
namespace App\EventListener;

use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Listener responsible for adding the default user role at registration
 */
class ProfileListener implements EventSubscriberInterface
{
    protected $router;

    public function __construct(UrlGeneratorInterface $router) {
        $this->router = $router;
    }

    public static function getSubscribedEvents() {
        return [
            FOSUserEvents::PROFILE_EDIT_SUCCESS => 'onProfileEditSuccess',
        ];
    }

    public function onProfileEditSuccess(FormEvent $event) {
        $url = $this->router->generate('account');
        $event->setResponse(new RedirectResponse($url));
    }
}