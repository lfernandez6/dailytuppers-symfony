<?php
namespace App\Form\Type;

use App\Entity\Category;
use App\Entity\Ingredient;
use App\Entity\Tupper;
use App\Utils\Difficulty;
use App\Utils\Duration;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TupperType extends AbstractType
{
    protected $images_directory;

    /**
     * TupperType constructor.
     */
    public function __construct(string $images_directory)
    {
        $this->images_directory = $images_directory;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', null, [
            'label' => 'Título del tupper',
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Título del tupper',
            ]
        ]);

        $builder->add('category', EntityType::class, [
            'class' => Category::class,
            'label' => 'Categoría',
            'placeholder' => '--',
            'attr' => [
                'class' => 'form-control',
            ],
        ]);


        $builder->add('difficulty', ChoiceType::class, [
            'label' => 'Dificultad',
            'choices' => Difficulty::getValues(),
            'attr' => [
                'class' => 'form-control',
            ],
            'placeholder' => '--',
        ]);

        $builder->add('duration', ChoiceType::class, [
            'label' => 'Duración',
            'choices' => Duration::getValues(),
            'attr' => [
                'class' => 'form-control',
            ],
            'placeholder' => '--',
        ]);

        //ingredientes
        $builder->add('ingredients', CollectionType::class, [
            'entry_type' => TuppersIngredientsType::class,
            'by_reference' => false,
            'allow_add' => true,
            'allow_delete' => true,
            'delete_empty' => true,
        ]);

        //comensales
        $builder->add('commensals', IntegerType::class, [
            'empty_data'       => 2,
            'label' => 'Número de comensales',
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Número de comensales',
            ]
        ]);

        //steps
        $builder->add('steps', TextareaType::class, [
            'label' => 'Pasos a seguir para su elaboración',
            'attr' => [
                'class' => 'form-control',
                'rows' => 8,
            ]
        ]);

        //imagen
        $builder->add('image', FileType::class, [
            'label' => 'Imagen (dimensiones mínimas: 1200x450, tamaño máximo: 1.5M)',
            'attr' => [
                'class' => 'form-control-file',
            ],
//            "data_class" => null,
        ]);

        //tags
        $builder->add('tags', CollectionType::class, [
            'entry_type' => TagType::class,
            'by_reference' => false,
            'allow_add' => true,
            'allow_delete' => true,
            'delete_empty' => true,
        ]);

        //event
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event){
            $data = $event->getData();

            //clear empty tags
            if(isset($data['tags'])) {
                foreach ($data['tags'] as $key => $tag) {
                    if (!isset($tag['slug']) || $tag['slug'] == "") {
                        unset($data['tags'][$key]);
                    }
                }
                $event->setData($data);
            }

            //clear empty ingredients
            if(isset($data['ingredients'])) {
                foreach ($data['ingredients'] as $key => $ingredient) {
                    if (!isset($ingredient['quantity']) || $ingredient['quantity'] == "") {
                        unset($data['ingredients'][$key]);
                    }
                }
                $event->setData($data);
            }
        });

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event){
            /** @var Tupper $tupper */
            $tupper = $event->getData();
            if(!empty($tupper->getImage())){
                $image = $this->images_directory.'/'.$tupper->getImage();
                if (file_exists($image)) {
                    $file = new File($image);
                    $tupper->setImage($file);
                    $event->setData($tupper);
                }
            }
        });



    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tupper::class,
        ]);
    }
}