<?php
namespace App\Form\Type;

use App\Entity\Ingredient;
use App\Entity\TuppersIngredients;
use PUGX\AutocompleterBundle\Form\Type\AutocompleteType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TuppersIngredientsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('quantity', null, [
            'attr' => [
                'class' => 'form-control w-auto mr-3',
                'placeholder' => 'Cantidad (Ej. 30 g)',
            ]
        ]);
//        $builder->add('ingredient', EntityType::class, [
//            'class' => Ingredient::class,
//            'label' => 'Ingredient',
//            'placeholder' => '--',
//            'attr' => [
//                'class' => 'form-control',
//            ],
//        ]);
        $builder->add('ingredient', AutocompleteType::class, [
            'class' => Ingredient::class,
            'attr' => [
                'class' => 'form-control w-auto mr-3',
                'placeholder' => 'Ingrediente (Ej. azúcar)',
            ],
        ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TuppersIngredients::class,
        ]);
    }
}
