<?php
namespace App\Form\Type;

use App\Entity\Ingredient;
use App\Entity\IngredientGroup;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IngredientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, [
            'label' => 'Nombre del ingrediente',
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Ingrediente',
            ]
        ]);

        $builder->add('group', EntityType::class, [
            'class' => IngredientGroup::class,
            'label' => 'Grupo',
            'placeholder' => '--',
            'attr' => [
                'class' => 'form-control',
            ],
        ]);

        $builder->add('calories', IntegerType::class, [
            'empty_data'       => 0,
            'label' => 'Calorías',
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Calorías',
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ingredient::class,
        ]);
    }
}
