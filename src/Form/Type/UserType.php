<?php
namespace App\Form\Type;

use App\Entity\Ingredient;
use App\Entity\IngredientGroup;
use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, [
            'label' => 'Nombre',
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Nombre',
            ]
        ]);

        $builder->add('surname', null, [
            'label' => 'Apellido',
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Apellido',
            ]
        ]);

        $builder->add('email', null, [
            'label' => 'Email',
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Email',
            ]
        ]);


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
