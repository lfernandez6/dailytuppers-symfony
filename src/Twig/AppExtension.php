<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 20/04/2019
 * Time: 13:33
 */

namespace App\Twig;

use App\Entity\Category;
use App\Entity\IngredientGroup;
use App\Entity\Tupper;
use App\Repository\IngredientGroupRepository;
use App\Repository\TupperRepository;
use App\Utils\Difficulty;
use App\Utils\Duration;
use App\Utils\SocialManager;
use App\Utils\Utils;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{

    protected $doctrine;

    /**
     * AppExtension constructor.
     */
    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getFilters()
    {
        return [
//            new TwigFilter('price', [$this, 'formatPrice']),
        ];
    }

//    public function formatPrice($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
//    {
//        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
//        $price = '$'.$price;
//
//        return $price;
//    }

    public function getFunctions()
    {
        return [
//            new TwigFunction('area', [$this, 'calculateArea']),
//            new TwigFunction('topCategories', [ListController::class, 'topCategories']),
            new TwigFunction('topCategories', [$this, 'topCategories']),
            new TwigFunction('getCategories', [$this, 'getCategories']),
            new TwigFunction('getDifficulty', [$this, 'getDifficulty']),
            new TwigFunction('getDuration', [$this, 'getDuration']),
            new TwigFunction('getIngredientGroups', [$this, 'getIngredientGroups']),
            new TwigFunction('socialFB', [$this, 'socialFB']),
            new TwigFunction('socialTW', [$this, 'socialTW']),
            new TwigFunction('socialWhatsapp', [$this, 'socialWhatsapp']),
            new TwigFunction('cutText', [$this, 'cutText']),
            new TwigFunction('isMobile', [$this, 'isMobile']),
        ];
    }

//    public function calculateArea(int $width, int $length)
//    {
//        return $width * $length;
//    }
    public function topCategories()
    {
        /** @var TupperRepository $repository */
        $repository = $this->doctrine->getRepository(Tupper::class);
        return $repository->findTopCategories();

    }

    public function getCategories()
    {
        /** @var TupperRepository $repository */
        $repository = $this->doctrine->getRepository(Category::class);
        return $repository->findAll();

    }

    public function getDifficulty()
    {
        return Difficulty::getValues();
    }

    public function getDuration()
    {
        return Duration::getValues();
    }

    public function getIngredientGroups()
    {
        /** @var IngredientGroupRepository $repository */
        $repository = $this->doctrine->getRepository(IngredientGroup::class);
        return $repository->findAll();

    }

    public function socialFB($url){
        return SocialManager::makeFB($url);
    }
    public function socialTW($url, $title){
        return SocialManager::makeTw($url, $title);
    }
    public function socialWhatsapp($url, $title){
        return SocialManager::makeWhatsapp($url, $title);
    }

    public function cutText($text, $length){
        return Utils::getExcerpt($text, $length);
    }

    public function isMobile(){
        $detect = new \Mobile_Detect();
        return $detect->isMobile();
    }
}