<?php
/**
 * Created by PhpStorm.
 * User: laura
 * Date: 9/04/19
 * Time: 21:40
 */

namespace App\Utils;


final class Difficulty
{
    const LOW = 1;
    const MEDIUM = 2;
    const HIGHT = 3;

    static function getDescription(int $difficulty) : string
    {
        switch ($difficulty){
            case self::LOW:
                return "Baja";

            case self::MEDIUM:
                return 'Media';

            case self::HIGHT:
                return 'Alta';

            default:
                return 'Desconocida';
        }
    }

    static function getValues() : array
    {
        return [
            self::getDescription(self::LOW) => self::LOW,
            self::getDescription(self::MEDIUM) => self::MEDIUM,
            self::getDescription(self::HIGHT) => self::HIGHT,
        ];
    }
}