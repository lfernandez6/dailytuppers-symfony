<?php
/**
 * Created by PhpStorm.
 * User: laura
 * Date: 9/04/19
 * Time: 21:41
 */

namespace App\Utils;


final class Duration
{
    const DURATION_15       = '15 minutos';
    const DURATION_30       = '30 minutos';
    const DURATION_45       = '45 minutos';
    const DURATION_60       = '60 minutos';
    const DURATION_MAS_60   = '+ 60 minutos';

    static function getValues() : array
    {
        return [
            self::DURATION_15 => self::DURATION_15,
            self::DURATION_30 => self::DURATION_30,
            self::DURATION_45 => self::DURATION_45,
            self::DURATION_60 => self::DURATION_60,
            self::DURATION_MAS_60 => self::DURATION_MAS_60,
        ];
    }
}