<?php


namespace App\Utils;


class Utils
{
    static public function getExcerpt(string $text, int $max_length = 200) : string
    {
        if (strlen($text) > $max_length){
            $offset = ($max_length - 3) - strlen($text);
            $text = substr($text, 0, strrpos($text, ' ', $offset)) . '...';
        }
        return $text;
    }
}