<?php


namespace App\Utils;


class SocialManager
{
    /**
     * @param string $url
     * @return string
     */
    static public function makeFB(string $url) : string
    {
        $url = urlencode($url);
        return "https://www.facebook.com/sharer/sharer.php?u={$url}";
    }

    static public function makeTw(string $url, string $title = "") : string
    {
        $url = urlencode($url);
        return "https://twitter.com/intent/tweet?url={$url}&text={$title}";
    }

    static public function makeWhatsapp(string $url, string $title = ""): string
    {
        $url = urlencode($url);
        return "whatsapp://send?text={$title} {$url}";
    }
}