<?php

namespace App\Controller;

use App\Entity\Tupper;
use App\Entity\User;
use App\Repository\TupperRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;


class AccountController extends AbstractController
{
    protected $limit = 8;
    protected $sort;
    protected $search;
    protected $category;
    protected $difficulty;
    protected $duration;

    /**
     * AccountController constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $request = $requestStack->getCurrentRequest();

        $sort = $request->query->get('ordenar');
        $this->sort = (null == $sort) ? 'last' : $sort;

        $this->search = $request->query->get('buscar');
        $this->category = $request->query->get('categoria');
        $this->difficulty = $request->query->get('dificultad');
        $this->duration = $request->query->get('duracion');
    }

    /**
     * @return Response
     */
    public function index()
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var TupperRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Tupper::class);

        //Last created
        /** @var Paginator $tuppersLastPaginator */
        $tuppersLastPaginator = $repository->findLast(null,10, $user->getId());
        $tuppersLast = $tuppersLastPaginator->getQuery()->getResult();

        //Most Viewed
        /** @var Paginator $tuppersVisitedPaginator */
        $tuppersVisitedPaginator = $repository->findMostVisited(null, 4, $user->getId());
        $tuppersVisited = $tuppersVisitedPaginator->getQuery()->getResult();

        //Most Rated
        /** @var Paginator $tuppersRatedPaginator */
        $tuppersRatedPaginator = $repository->findMostRated(null, 4, $user->getId());
        $tuppersRated = $tuppersRatedPaginator->getQuery()->getResult();
        $tuppersRated = $repository->parseResults($tuppersRated);

        return $this->render('account/my-account.html.twig', [
            'user' => $user,
            'last_tuppers' => $tuppersLast,
            'visited_tuppers' => $tuppersVisited,
            'rated_tuppers' => $tuppersRated,
        ]);
    }

    /**
     * @param int $currentPage
     * @return Response
     */
    public function myTuppers(int $currentPage = 1)
    {
        $this->sort = 'recientes';

        /** @var User $user */
        $user = $this->getUser();

        /** @var TupperRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Tupper::class);

        //Last created
        /** @var Paginator $tuppersPaginator */
        $tuppersPaginator = $repository->findLast($currentPage, $this->limit, $user->getId(), $this->search, $this->category, $this->difficulty, $this->duration);
        $tuppers = $tuppersPaginator->getQuery()->getResult();

        $totalItems = $tuppersPaginator->count();
        $maxPages = ceil($totalItems / $this->limit);

        return $this->render('account/my-tuppers.html.twig', [
            'page_title' => 'Mis tuppers',
            'page_type' => 'user-list',
            'tuppers' => $tuppers,
            'maxPages'  => $maxPages,
            'thisPage'  => $currentPage,
            'filter_vars' => [
                'sort'          =>  $this->sort,
                'search'        =>  $this->search,
                'category'      =>  $this->category,
                'difficulty'    =>  $this->difficulty,
                'duration'      =>  $this->duration,
            ],
        ]);
    }

    /**
     * @param int $currentPage
     * @return Response
     */
    public function myTuppersVisited(int $currentPage = 1)
    {
        $this->sort = 'visitados';

        /** @var User $user */
        $user = $this->getUser();

        /** @var TupperRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Tupper::class);

        //Most Viewed
        /** @var Paginator $tuppersPaginator */
        $tuppersPaginator = $repository->findMostVisited($currentPage, $this->limit, $user->getId(), $this->search, $this->category, $this->difficulty, $this->duration);
        $tuppers = $tuppersPaginator->getQuery()->getResult();

        $totalItems = $tuppersPaginator->count();
        $maxPages = ceil($totalItems / $this->limit);

        return $this->render('account/my-tuppers.html.twig', [
            'page_title' => 'Mis tuppers más visitados',
            'page_type' => 'user-list',
            'tuppers' => $tuppers,
            'maxPages'  => $maxPages,
            'thisPage'  => $currentPage,
            'filter_vars' => [
                'sort'          =>  $this->sort,
                'search'        =>  $this->search,
                'category'      =>  $this->category,
                'difficulty'    =>  $this->difficulty,
                'duration'      =>  $this->duration,
            ],
        ]);
    }

    /**
     * @param int $currentPage
     * @return Response
     */
    public function myTuppersRated(int $currentPage = 1)
    {
        $this->sort = 'valorados';

        /** @var User $user */
        $user = $this->getUser();

        /** @var TupperRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Tupper::class);

        //Most Rated
        /** @var Paginator $tuppersPaginator */
        $tuppersPaginator = $repository->findMostRated($currentPage, $this->limit, $user->getId(), $this->search, $this->category, $this->difficulty, $this->duration);
        $tuppers = $tuppersPaginator->getQuery()->getResult();
        $tuppers = $repository->parseResults($tuppers);

        $totalItems = $tuppersPaginator->count();
        $maxPages = ceil($totalItems / $this->limit);

        return $this->render('account/my-tuppers.html.twig', [
            'page_title' => 'Mis tuppers mejor valorados',
            'page_type' => 'user-list',
            'tuppers' => $tuppers,
            'maxPages'  => $maxPages,
            'thisPage'  => $currentPage,
            'filter_vars' => [
                'sort'          =>  $this->sort,
                'search'        =>  $this->search,
                'category'      =>  $this->category,
                'difficulty'    =>  $this->difficulty,
                'duration'      =>  $this->duration,
            ],
        ]);
    }


}
