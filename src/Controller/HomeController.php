<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Rating;
use App\Entity\Tag;
use App\Entity\Tupper;
use App\Repository\CategoryRepository;
use App\Repository\RatingRepository;
use App\Repository\TupperRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController
{

    /**
     * @return Response
     */
    public function index()
    {
        /** @var TupperRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Tupper::class);

        //Last created
        $tuppersLastPaginator = $repository->findLast(null, 5);
        $tuppersLast = $tuppersLastPaginator->getQuery()->getResult();

        //Most Viewed
        $tuppersVisitedPaginator = $repository->findMostVisited(null,4);
        $tuppersVisited = $tuppersVisitedPaginator->getQuery()->getResult();

        //Most Rated
        $tuppersRatedPaginator = $repository->findMostRated(null,4);
        $tuppersRated = $tuppersRatedPaginator->getQuery()->getResult();
        $tuppersRated = $repository->parseResults($tuppersRated);

        return $this->render('home/index.html.twig', [
            'last_tuppers' => $tuppersLast,
            'visited_tuppers' => $tuppersVisited,
            'rated_tuppers' => $tuppersRated,
        ]);
    }

}
