<?php

namespace App\Controller;

use App\Entity\Ingredient;
use App\Entity\Tupper;
use App\Repository\IngredientRepository;
use App\Repository\TupperRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class IngredientSearchController extends AbstractController
{
    protected $limit = 8;
    protected $sort;
    protected $search;
    protected $category;
    protected $difficulty;
    protected $duration;
    protected $ingredients;

    /**
     * IngredientSearchController constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $request = $requestStack->getCurrentRequest();

        $sort = $request->query->get('ordenar');
        $this->sort = (null == $sort) ? 'last' : $sort;

        $this->search = $request->query->get('buscar');
        $this->category = $request->query->get('categoria');
        $this->difficulty = $request->query->get('dificultad');
        $this->duration = $request->query->get('duracion');

        $ingredients = $request->query->get('ingredients');
        $this->ingredients = (null != $ingredients) ? explode(',', $ingredients) : [];
    }

    /**
     * @param int $currentPage
     * @return Response
     */
    public function index(int $currentPage = 1)
    {
        /** @var TupperRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Tupper::class);

        /** @var Paginator $tuppersPaginator */
        $tuppersPaginator = $repository->findByIngredient($this->ingredients, $currentPage, $this->limit, $this->sort, $this->search, $this->category, $this->difficulty, $this->duration);
        $tuppers = $tuppersPaginator->getQuery()->getResult();

        if($this->sort == 'valorados'){
            $tuppers = $repository->parseResults($tuppers);
        }

        $totalItems = $tuppersPaginator->count();
        $maxPages = ceil($totalItems / $this->limit);

        /** @var IngredientRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Ingredient::class);

        /** @var Paginator $tuppersPaginator */
        $ingredientsObj = $repository->findBy(['id' => $this->ingredients]);

        return $this->render('ingredient_search/index.html.twig', [
            'tuppers'   =>  $tuppers,
            'page_type' => 'ingredient-list',
            'maxPages'  => $maxPages,
            'thisPage'  => $currentPage,
            'filter_vars' => [
                'sort'        =>  $this->sort,
                'search'        =>  $this->search,
                'category'      =>  $this->category,
                'difficulty'    =>  $this->difficulty,
                'duration'      =>  $this->duration,
                'ingredients'   =>  $ingredientsObj,
            ],
        ]);
    }

    public function searchIngredient(Request $request)
    {
        $q = $request->query->get('q');

        /** @var IngredientRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Ingredient::class);
        $results = $repository->findLike($q);

        return $this->render('ingredient_search/autocomplete-ingredients.json.twig', ['results' => $results]);
    }

    public function getIngredient($id = null)
    {
        /** @var Ingredient $ingredient */
        $ingredient = $this->getDoctrine()->getRepository(Ingredient::class)->find($id);

        return $this->json($ingredient->getName());
    }

}
