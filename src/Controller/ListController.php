<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Tag;
use App\Entity\Tupper;
use App\Repository\CategoryRepository;
use App\Repository\TagRepository;
use App\Repository\TupperRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class ListController extends AbstractController
{
    protected $limit = 8;
    protected $sort;
    protected $search;
    protected $category;
    protected $difficulty;
    protected $duration;

    /**
     * ListController constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $request = $requestStack->getCurrentRequest();

        $sort = $request->query->get('ordenar');
        $this->sort = (null == $sort) ? 'last' : $sort;

        $this->search = $request->query->get('buscar');
        $this->category = $request->query->get('categoria');
        $this->difficulty = $request->query->get('dificultad');
        $this->duration = $request->query->get('duracion');
    }


    /**
     * @param int $currentPage
     * @return Response
     */
    public function list(int $currentPage = 1)
    {
        $this->sort = 'recientes';

        /** @var TupperRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Tupper::class);

        //Last created
        /** @var Paginator $tuppersPaginator */
        $tuppersPaginator = $repository->findLast($currentPage, $this->limit, null, $this->search, $this->category, $this->difficulty, $this->duration);
        $tuppers = $tuppersPaginator->getQuery()->getResult();

        $totalItems = $tuppersPaginator->count();
        $maxPages = ceil($totalItems / $this->limit);

        return $this->render('list/index.html.twig', [
            'page_title' => 'Explorar tuppers',
            'page_type' => 'general-list',
            'tuppers'   => $tuppers,
            'maxPages'  => $maxPages,
            'thisPage'  => $currentPage,
            'filter_vars' => [
                'sort'          =>  $this->sort,
                'search'        =>  $this->search,
                'category'      =>  $this->category,
                'difficulty'    =>  $this->difficulty,
                'duration'      =>  $this->duration,
            ],
        ]);
    }

    /**
     * @param int $currentPage
     * @return Response
     */
    public function listRated(int $currentPage = 1)
    {
        $this->sort = 'valorados';

        /** @var TupperRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Tupper::class);

        //Most Rated
        $tuppersPaginator = $repository->findMostRated($currentPage,$this->limit, null, $this->search, $this->category, $this->difficulty, $this->duration);
        $tuppers = $tuppersPaginator->getQuery()->getResult();
        $tuppers = $repository->parseResults($tuppers);

        $totalItems = $tuppersPaginator->count();
        $maxPages = ceil($totalItems / $this->limit);

        return $this->render('list/index.html.twig', [
            'page_title' => 'Tuppers mejor valorados',
            'page_type' => 'general-list',
            'tuppers' => $tuppers,
            'maxPages'  => $maxPages,
            'thisPage'  => $currentPage,
            'filter_vars' => [
                'sort'          =>  $this->sort,
                'search'        =>  $this->search,
                'category'      =>  $this->category,
                'difficulty'    =>  $this->difficulty,
                'duration'      =>  $this->duration,
            ],
        ]);
    }

    /**
     * @param int $currentPage
     * @return Response
     */
    public function listVisited(int $currentPage = 1)
    {
        $this->sort = 'visitados';

        /** @var TupperRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Tupper::class);

        //Most Viewed
        /** @var Paginator $tuppersPaginator */
        $tuppersPaginator = $repository->findMostVisited($currentPage,$this->limit, null, $this->search, $this->category, $this->difficulty, $this->duration);
        $tuppers = $tuppersPaginator->getQuery()->getResult();

        $totalItems = $tuppersPaginator->count();
        $maxPages = ceil($totalItems / $this->limit);

        return $this->render('list/index.html.twig', [
            'page_title' => 'Tuppers más visitados',
            'page_type' => 'general-list',
            'tuppers'   => $tuppers,
            'maxPages'  => $maxPages,
            'thisPage'  => $currentPage,
            'filter_vars' => [
                'sort'          =>  $this->sort,
                'search'        =>  $this->search,
                'category'      =>  $this->category,
                'difficulty'    =>  $this->difficulty,
                'duration'      =>  $this->duration,
            ],
        ]);
    }

    /**
     * @param string $slug
     * @param int $currentPage
     * @return Response
     */
    public function listCategory(string $slug, int $currentPage = 1)
    {
        /** @var CategoryRepository $repository */
        $repositoryCategory = $this->getDoctrine()->getRepository(Category::class);
        /** @var Category $category */
        $category = $repositoryCategory->findOneBySlug($slug);

        if(null == $category){
            throw $this->createNotFoundException("La categoría solicitada no existe.");
        }

        /** @var TupperRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Tupper::class);

        //Tuppers category list
        /** @var Paginator $tuppersPaginator */
        $tuppersPaginator = $repository->findByCategory($slug, $currentPage, $this->limit, $this->sort, $this->search, $this->category, $this->difficulty, $this->duration);
        $tuppers = $tuppersPaginator->getQuery()->getResult();

        if($this->sort == 'valorados'){
            $tuppers = $repository->parseResults($tuppers);
        }

        $totalItems = $tuppersPaginator->count();
        $maxPages = ceil($totalItems / $this->limit);

        return $this->render('list/index.html.twig', [
            'page_title' => 'Tuppers de la categoria <span class="green bold">' .$category->getName(). '</span>',
            'page_type' => 'category-list',
            'tuppers' => $tuppers,
            'maxPages'  => $maxPages,
            'thisPage'  => $currentPage,
            'withParams' => ['slug' => $slug],
            'filter_vars' => [
                'sort'          =>  $this->sort,
                'search'        =>  $this->search,
                'category'      =>  $this->category,
                'difficulty'    =>  $this->difficulty,
                'duration'      =>  $this->duration,
            ],
        ]);
    }

    /**
     * @param string $slug
     * @param int $currentPage
     * @return Response
     */
    public function listTag(string $slug, int $currentPage = 1)
    {
        /** @var TagRepository $repository */
        $repositoryTag = $this->getDoctrine()->getRepository(Tag::class);
        /** @var Tag $tag */
        $tag = $repositoryTag->findOneBySlug($slug);

        if(null == $tag){
            throw $this->createNotFoundException("El tag solicitado no existe.");
        }

        /** @var TupperRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Tupper::class);

        //Tuppers tag list
        /** @var Paginator $tuppersPaginator */
        $tuppersPaginator = $repository->findByTag($slug, $currentPage, $this->limit, $this->sort, $this->search, $this->category, $this->difficulty, $this->duration);
        $tuppers = $tuppersPaginator->getQuery()->getResult();

        if($this->sort == 'valorados'){
            $tuppers = $repository->parseResults($tuppers);
        }

        $totalItems = $tuppersPaginator->count();
        $maxPages = ceil($totalItems / $this->limit);

        return $this->render('list/index.html.twig', [
            'page_title' => 'Tuppers con la etiqueta <span class="green bold">' .$slug. '</span>',
            'page_type' => 'tag-list',
            'tuppers' => $tuppers,
            'maxPages'  => $maxPages,
            'thisPage'  => $currentPage,
            'withParams' => ['slug' => $slug],
            'filter_vars' => [
                'sort'          =>  $this->sort,
                'search'        =>  $this->search,
                'category'      =>  $this->category,
                'difficulty'    =>  $this->difficulty,
                'duration'      =>  $this->duration,
            ],
        ]);
    }
}
