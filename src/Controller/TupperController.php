<?php

namespace App\Controller;

use App\Entity\Rating;
use App\Entity\Tupper;
use App\Entity\User;
use phpDocumentor\Reflection\Types\This;
use App\Repository\RatingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Ingredient;
use App\Entity\Tag;
use App\Form\Type\TupperType;
use App\Entity\TuppersIngredients;
use Doctrine\DBAL\DBALException;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use DateTime;

class TupperController extends AbstractController
{
    /**
     * @param string $slug
     * @return Response
     */
    public function index(string $slug)
    {
        $showEditButton = false;

        /** @var User $user */
        $user = $this->getUser();

        $repository = $this->getDoctrine()->getRepository(Tupper::class);
        /** @var Tupper $tupper */
        $tupper = $repository->findOneBy(['slug' => $slug]);

        if(null == $tupper){
            throw $this->createNotFoundException('El tupper solicitado no existe.');
        }

        if((null === $user && !$tupper->isPublished()) || (null !== $user && !$tupper->isPublished() && !in_array('ROLE_SUPER_ADMIN', $user->getRoles()))){
            throw $this->createNotFoundException('El tupper solicitado no existe.');
        }

        if(null !== $user && $user->getId() == $tupper->getUser()->getId()){
            $showEditButton = true;
        }

        $ratingRepository = $this->getDoctrine()->getRepository(Rating::class);
        /** @var RatingRepository $rate */
        $rate = $ratingRepository->findOneBy(['user' => $user, 'tupper' => $tupper]);

        //count visit
        $this->countVisit($tupper);

        return $this->render('tupper/index.html.twig', [
            'tupper' => $tupper,
            'showEdit' => $showEditButton,
            'rate' => (null === $rate) ? 0 : $rate->getRate(),
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function create(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        $tupper = new Tupper();

        //Create form
        $form = $this->createForm(TupperType::class, $tupper, [
            'action' => $this->generateUrl('tupper_create'),
        ]);

        //get form submit
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                $tupper->setUser($user);
                $tupper->setCreationDate(new DateTime());

                //check if slug is unique
                $isUniqueSlug = $this->isUniqueSlug($tupper);
                if(!$isUniqueSlug){
                    $slug = $tupper->getSlug().uniqid();
                    $tupper->setSlug($slug);
                }

                //manage image
                /** @var UploadedFile $file */
                $file = $request->files->get('tupper')['image'];
                $image = $this->saveImage($file, $tupper->getSlug());
                $tupper->setImage($image);

                //manage tags
                $requestTags = $tupper->getTags();
                if(!empty($requestTags)){
                    $repositoryTag = $this->getDoctrine()->getRepository(Tag::class);

                    foreach ($requestTags as $key=>$tag) {
                        /** @var Tag $tagDB */
                        $tagDB = $repositoryTag->findOneBy(['slug' => $tag->getSlug()]);
                        //if not empty, assign existing tag
                        if(null != $tagDB){
                            $requestTags->set($key, $tagDB);
                            $tagDB->addTupper($tupper);
                        }
                    }
                }

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($tupper);
                $entityManager->flush();

                return $this->redirectToRoute('account_mytuppers', [
                    'id' => $tupper->getId()
                ]);

            }catch(DBALException $e){
                if(1062 == $e->getErrorCode()){
                    $form->addError(new FormError('Ha habido un problema al guadar el tupper en Base de datos.'));
                }

            }catch (FileException $e) {
                $form->addError(new FormError('Error guardando la imagen.'));

            }catch (\Exception $e){
//                $form->addError(new FormError('Error inesperado.'));
                $form->addError(new FormError($e->getMessage()));
            }
        }

        return $this->render('tupper/form-tupper-create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @return Response
     */
    public function edit(Request $request, int $tupperid, EntityManagerInterface $entityManager)
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var Tupper $tupper */
        $tupper = $entityManager->getRepository(Tupper::class)->find($tupperid);

        //tupper does not exist
        if(null == $tupper){
            throw $this->createNotFoundException('El tupper que quieres editar no existe.');
        }
        $oldImage = $tupper->getImage();

        //user does not own this tupper
        if(!in_array('ROLE_SUPER_ADMIN', $user->getRoles()) && ($tupper->getUser()->getId() !== $user->getId())){
            throw new AccessDeniedException('No tienes permisos para editar este tupper');
        }

        //Create form
        $form = $this->createForm(TupperType::class, $tupper, [
            'action' => $this->generateUrl('tupper_edit', ['tupperid' => $tupperid]),
        ]);

        $form->remove('image');
        $form->add('image', FileType::class, [
            'label' => 'Imagen (dimensiones mínimas: 1500x450)',
            'attr' => [
                'class' => 'form-control-file',
            ],
            'required' => false,
//            "data_class" => null,
        ]);

        //get form submit
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $tupper->setUpdateDate(new DateTime());

                //check if slug is unique
                $isUniqueSlug = $this->isUniqueSlug($tupper);
                if(!$isUniqueSlug){
                    $slug = $tupper->getSlug().uniqid();
                    $tupper->setSlug($slug);
                }

                //manage image
                /** @var UploadedFile $file */
                $file = $request->files->get('tupper')['image'];
                if($file instanceof UploadedFile){
                    $image = $this->saveImage($file, $tupper->getSlug());
                    $tupper->setImage($image);
                    $tupper->setOverrideImage(true);
                }else{
                    $tupper->setImage($oldImage);
                }


                //manage tags
                $requestTags = $tupper->getTags();
                if(!empty($requestTags)){
                    $repositoryTag = $this->getDoctrine()->getRepository(Tag::class);

                    foreach ($requestTags as $key=>$tag) {
                        /** @var Tag $tagDB */
                        $tagDB = $repositoryTag->findOneBy(['slug' => $tag->getSlug()]);
                        //if not empty, assign existing tag
                        if(null != $tagDB){
                            $requestTags->set($key, $tagDB);
                            $tagDB->addTupper($tupper);
                        }
                    }
                }

                $entityManager->flush();

                //delete old image
                if(true === $tupper->getOverrideImage()){
                    $this->deleteImage($oldImage);
                }


                if(null !== $user && in_array('ROLE_SUPER_ADMIN', $user->getRoles())){
                    return $this->redirectToRoute('admin_tuppers');
                }else{
                    return $this->redirectToRoute('account_mytuppers');
                }

            }catch(DBALException $e){
                if(1062 == $e->getErrorCode()){
                    $form->addError(new FormError('Ha habido un problema al guadar el tupper en Base de datos.'));
                }

            }catch (FileException $e) {
                $form->addError(new FormError('Error guardando la imagen.'));

            }catch (\Exception $e){
//                $form->addError(new FormError('Error inesperado.'));
                $form->addError(new FormError($e->getMessage()));
            }
        }



        return $this->render('tupper/form-tupper-create.html.twig', [
            'form' => $form->createView(),
            'old_image' => $oldImage,
            'edit_form' => true,
        ]);
    }

    public function delete(Request $request, $tupperid){
        try{
            /** @var User $user */
            $user = $this->getUser();

            /** @var Tupper $tupper */
            $tupper = $this->getDoctrine()->getRepository(Tupper::class)
                ->find($tupperid);

            if(!in_array('ROLE_SUPER_ADMIN', $user->getRoles()) && ($tupper->getUser()->getId() !== $user->getId())){
                throw new AccessDeniedException('No tienes permisos para editar este tupper');
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tupper);
            $entityManager->flush();

            $resp = [
                'code'      =>  Response::HTTP_OK,
                'message'   =>  "Tupper eliminado correctamente."
            ];

        }catch(AccessDeniedException $e){
            $resp = [
                'code'      =>  Response::HTTP_UNAUTHORIZED,
                'message'   =>  $e->getMessage()
            ];
        }catch(\Exception $e){
            $resp = [
                'code'      =>  Response::HTTP_FORBIDDEN,
                'message'   =>  $e->getMessage()
            ];
        }

        return new JsonResponse(['data' => $resp['message']], $resp['code']);
    }

    public function rate(Request $request, $tupperid, $rate){
        try{
            /** @var User $user */
            $user = $this->getUser();

            if (null == $user){
                throw new AccessDeniedException('Debes haber iniciado sesión para votar');
            }

            /** @var Tupper $tupper */
            $tupper = $this->getDoctrine()->getRepository(Tupper::class)
                ->find($tupperid);

            /** @var Rating $rating */
            $rating = $this->getDoctrine()->getRepository(Rating::class)
                ->findOneBy(['tupper' => $tupper, 'user' => $user]);

            if(null == $rating){
                $rating = new Rating();
                $rating
                    ->setTupper($tupper)
                    ->setUser($user)
                    ->setRate($rate);
            }else{
                $rating->setRate($rate);
            }

            $tupper->addRating($rating);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();


            $resp = [
                'code'      =>  Response::HTTP_OK,
                'message'   =>  $tupper->getTotalRate(),
            ];

        }catch(AccessDeniedException $e){
            $resp = [
                'code'      =>  Response::HTTP_UNAUTHORIZED,
                'message'   =>  $e->getMessage(),
            ];
        }catch(\Exception $e){
            $resp = [
                'code'      =>  Response::HTTP_FORBIDDEN,
                'message'   =>  $e->getMessage(),
            ];
        }

        return new JsonResponse(['data' => $resp['message']], $resp['code']);
    }

    public function countVisit(Tupper $tupper){
        try{
            $visits = $tupper->getVisits();
            $visits++;
            $tupper->setVisits($visits);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

        }catch(AccessDeniedException $e){

        }catch(\Exception $e){

        }
    }

    /**
     * Check if slug already exist.
     * If exist, return false
     * Else, return true
     *
     * @param Tupper $tupper
     * @return bool
     */
    private function isUniqueSlug(Tupper $tupper){
        $em = $this->getDoctrine()->getManager();
        /** @var Tupper $found */
        $found = $em->getRepository(Tupper::class)->findOneBy(['slug' => $tupper->getSlug()]);
        if(null !== $found && $found->getId() !== $tupper->getId()){
            return false;
        }

        return true;
    }

    /**
     * Save image into uplodas folder
     *
     * @param UploadedFile $file
     * @param $name
     * @return string
     * @throws \Exception
     */
    private function saveImage(UploadedFile $file, $name){
        $fileName = "{$name}_".uniqid().".{$file->guessExtension()}";

        $imageSize = getimagesize($file);
        $width = $imageSize[0];
        $height = $imageSize[1];
        if($width < 1200 || $height < 450){
            throw new \Exception("La imagen debe tener unas dimensiones mínimas de: 1200x450");
        }
        if($width < $height){
            throw new \Exception("La imagen debe ser horizontal");
        }

        // Move the file to the directory where images are stored
        $file->move(
            $this->getParameter('images_directory'),
            $fileName
        );

        return $fileName;
    }

    /**
     * Delete image from uploads folder
     *
     * @param string $file
     */
    private function deleteImage(string $file){
        $path = $this->getParameter('images_directory');
        $image = "{$path}/{$file}";
        if (file_exists($image)) {
            unlink($image);
        }
    }
}
