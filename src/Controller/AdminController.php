<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Ingredient;
use App\Entity\Tupper;
use App\Entity\TuppersIngredients;
use App\Entity\User;
use App\Form\Type\CategoryType;
use App\Form\Type\IngredientType;
use App\Form\Type\UserType;
use App\Repository\CategoryRepository;
use App\Repository\IngredientRepository;
use App\Repository\TupperRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AdminController extends AbstractController
{
    protected $limit = 10;
    protected $group;
    protected $search;
    protected $sort;
    protected $category;
    protected $difficulty;
    protected $duration;

    /**
     * AdminController constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $request = $requestStack->getCurrentRequest();

        $group = $request->query->get('grupo');
        $this->group = (null == $group) ? '1' : $group;

        $this->search = $request->query->get('buscar');

        $sort = $request->query->get('ordenar');
        $this->sort = (null == $sort) ? 'last' : $sort;

        $this->category = $request->query->get('categoria');
        $this->difficulty = $request->query->get('dificultad');
        $this->duration = $request->query->get('duracion');
    }


    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $categories = $categoryRepository->getTotalTupperByCategory();

        /** @var UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $users = $userRepository->getUsersStatus();
        $users = $userRepository->parseStatusResults($users);

        /** @var TupperRepository $tupperRepository */
        $tupperRepository = $this->getDoctrine()->getRepository(Tupper::class);
        $tuppers = $tupperRepository->getTuppersStatus();
        $tuppers = $tupperRepository->parteStatusResults($tuppers);

        return $this->render('admin/index.html.twig', [
            'users'         =>  $users,
            'tuppers'       =>  $tuppers,
            'categories'    =>  $categories,
        ]);
    }

    /**
     * @param int $currentPage
     * @return Response
     */
    public function tuppers(int $currentPage = 1)
    {
        /** @var TupperRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Tupper::class);

        switch ($this->sort){
            case 'last':
            case 'recientes':
            default:
                /** @var Paginator $tuppersPaginator */
                $tuppersPaginator = $repository->findLast($currentPage, $this->limit, null, $this->search, $this->category, $this->difficulty, $this->duration, null);
                $tuppers = $tuppersPaginator->getQuery()->getResult();
                break;
            case 'valorados':
                /** @var Paginator $tuppersPaginator */
                $tuppersPaginator = $repository->findMostRated($currentPage, $this->limit, null, $this->search, $this->category, $this->difficulty, $this->duration, null);
                $tuppers = $tuppersPaginator->getQuery()->getResult();
                $tuppers = $repository->parseResults($tuppers);
                break;
            case 'visitados':
                /** @var Paginator $tuppersPaginator */
                $tuppersPaginator = $repository->findMostVisited($currentPage, $this->limit, null, $this->search, $this->category, $this->difficulty, $this->duration, null);
                $tuppers = $tuppersPaginator->getQuery()->getResult();
                break;
        }

        $totalItems = $tuppersPaginator->count();
        $maxPages = ceil($totalItems / $this->limit);

        return $this->render('admin/tuppers/index.html.twig', [
            'data' => $tuppers,
            'maxPages'  => $maxPages,
            'thisPage'  => $currentPage,
            'page_type'  => 'admin_list',
            'filter_vars' => [
                'search'      =>  $this->search,
                'category'      =>  $this->category,
                'difficulty'    =>  $this->difficulty,
                'duration'      =>  $this->duration,
                'sort'          =>  $this->sort,
            ],
        ]);
    }

    /**
     * @param Request $request
     * @param int $tupperid
     * @param bool $published
     * @return JsonResponse
     */
    public function publishTupper(Request $request, int $tupperid, bool $published){
        try{
            /** @var Tupper $tupper */
            $tupper = $this->getDoctrine()->getRepository(Tupper::class)
                ->find($tupperid);

            $tupper->setPublished($published);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            $resp = [
                'code'      =>  Response::HTTP_OK,
                'message'   =>  (true === $published) ? "Tupper publicado correctamente." : "Tupper despublicado correctamente."
            ];

        }catch(\Exception $e){
            $resp = [
                'code'      =>  Response::HTTP_FORBIDDEN,
                'message'   =>  $e->getMessage()
            ];
        }

        return new JsonResponse(['data' => $resp['message']], $resp['code']);
    }

    /**
     * @param int $currentPage
     * @return Response
     */
    public function users(int $currentPage = 1)
    {
        /** @var UserRepository $repository */
        $repository = $this->getDoctrine()->getRepository(User::class);

        $usersPaginator = $repository->getAllUsers($currentPage, $this->limit, $this->search);
        $users = $usersPaginator->getQuery()->getResult();

        $totalItems = $usersPaginator->count();
        $maxPages = ceil($totalItems / $this->limit);

        return $this->render('admin/users/index.html.twig', [
            'data' => $users,
            'maxPages'  => $maxPages,
            'thisPage'  => $currentPage,
            'filter_vars' => [
                'search'      =>  $this->search,
            ],
        ]);
    }

    /**
     * @param Request $request
     * @param int $userid
     * @param EntityManagerInterface $entityManager
     * @return RedirectResponse|Response
     */
    public function editUser(Request $request, int $userid, EntityManagerInterface $entityManager){

        /** @var User $user */
        $user = $entityManager->getRepository(User::class)->find($userid);

        //user does not exist
        if(null == $user){
            throw $this->createNotFoundException('El usuario que quieres editar no existe.');
        }

        //Create form
        $form = $this->createForm(UserType::class, $user, [
            'action' => $this->generateUrl('admin_users_edit', ['userid' => $userid]),
        ]);

        //get form submit
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {

                //check if slug is unique
                /** @var User $found */
                $found = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $user->getEmail()]);
                if(null !== $found && $found->getId() !== $user->getId()){
                    throw new \Exception('Este email ya existe.');
                }

                $entityManager->flush();

                return $this->redirectToRoute('admin_users');

            }catch(DBALException $e){
                if(1062 == $e->getErrorCode()){
                    $form->addError(new FormError('Ha habido un problema al guadar el usuario en Base de datos.'));
                }

            }catch (\Exception $e){
                $form->addError(new FormError($e->getMessage()));
            }
        }


        return $this->render('admin/users/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param $userid
     * @return JsonResponse
     */
    public function deleteUser(Request $request, $userid){
        try{
            /** @var User $user */
            $user = $this->getDoctrine()->getRepository(User::class)
                ->find($userid);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();

            $resp = [
                'code'      =>  Response::HTTP_OK,
                'message'   =>  "Usuario eliminado correctamente."
            ];

        }catch(\Exception $e){
            $resp = [
                'code'      =>  Response::HTTP_FORBIDDEN,
                'message'   =>  $e->getMessage()
            ];
        }

        return new JsonResponse(['data' => $resp['message']], $resp['code']);
    }

    /**
     * Block/Unblock user
     *
     * @param Request $request
     * @param int $userid
     * @param bool $banned
     * @return JsonResponse
     */
    public function blockUser(Request $request, int $userid, bool $banned){
        try{
            /** @var User $user */
            $user = $this->getDoctrine()->getRepository(User::class)
                ->find($userid);

            $user->setBanned($banned);

            if(true === $banned){
                $user->removeRole('ROLE_DEFAULT_USER');
            }else{
                $user->addRole('ROLE_DEFAULT_USER');
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            $resp = [
                'code'      =>  Response::HTTP_OK,
                'message'   =>  (true === $banned) ? "Usuario bloqueado correctamente." : "Usuario desbloqueado correctamente."
            ];

        }catch(\Exception $e){
            $resp = [
                'code'      =>  Response::HTTP_FORBIDDEN,
                'message'   =>  $e->getMessage()
            ];
        }

        return new JsonResponse(['data' => $resp['message']], $resp['code']);
    }

    /**
     * @param int $currentPage
     * @return Response
     */
    public function categories(int $currentPage = 1)
    {
        /** @var CategoryRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Category::class);

        $categoriesPaginator = $repository->getAllCategories($currentPage, $this->limit);
        $categories = $categoriesPaginator->getQuery()->getResult();

        $totalItems = $categoriesPaginator->count();
        $maxPages = ceil($totalItems / $this->limit);

        return $this->render('admin/categories/index.html.twig', [
            'data' => $categories,
            'maxPages'  => $maxPages,
            'thisPage'  => $currentPage,
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createCategory(Request $request){
        $category = new Category();

        //Create form
        $form = $this->createForm(CategoryType::class, $category, [
            'action' => $this->generateUrl('admin_categories_create'),
        ]);

        //get form submit
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                //check if slug is unique
                /** @var Category $found */
                $found = $this->getDoctrine()->getRepository(Category::class)->findOneBy(['slug' => $category->getSlug()]);
                if(null !== $found && $found->getId() !== $category->getId()){
                    $slug = $category->getSlug().uniqid();
                    $category->setSlug($slug);
                }

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($category);
                $entityManager->flush();

                return $this->redirectToRoute('admin_categories');

            }catch(DBALException $e){
                if(1062 == $e->getErrorCode()){
                    $form->addError(new FormError('Ha habido un problema al guadar la categoría en Base de datos.'));
                }

            }catch (\Exception $e){
                $form->addError(new FormError($e->getMessage()));
            }
        }

        return $this->render('admin/categories/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param int $categoryid
     * @param EntityManagerInterface $entityManager
     * @return RedirectResponse|Response
     */
    public function editCategory(Request $request, int $categoryid, EntityManagerInterface $entityManager){

        /** @var Category $ingredient */
        $category = $entityManager->getRepository(Category::class)->find($categoryid);

        //ingredient does not exist
        if(null == $category){
            throw $this->createNotFoundException('La categoría que quieres editar no existe.');
        }

        //Create form
        $form = $this->createForm(CategoryType::class, $category, [
            'action' => $this->generateUrl('admin_categories_edit', ['categoryid' => $categoryid]),
        ]);

        //get form submit
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {

                //check if slug is unique
                /** @var Category $found */
                $found = $this->getDoctrine()->getRepository(Category::class)->findOneBy(['slug' => $category->getSlug()]);
                if(null !== $found && $found->getId() !== $category->getId()){
                    $slug = $category->getSlug().uniqid();
                    $category->setSlug($slug);
                }

                $entityManager->flush();

                return $this->redirectToRoute('admin_categories');

            }catch(DBALException $e){
                if(1062 == $e->getErrorCode()){
                    $form->addError(new FormError('Ha habido un problema al guadar la categoría en Base de datos.'));
                }

            }catch (\Exception $e){
                $form->addError(new FormError($e->getMessage()));
            }
        }


        return $this->render('admin/categories/create.html.twig', [
            'form' => $form->createView(),
            'edit_form' => true,
        ]);
    }

    /**
     * @param Request $request
     * @param $categoryid
     * @return JsonResponse
     */
    public function deleteCategory(Request $request, $categoryid){
        try{
            /** @var Category $category */
            $category = $this->getDoctrine()->getRepository(Category::class)
                ->find($categoryid);

            /** @var TupperRepository $tupperRepository */
            $tupperRepository = $this->getDoctrine()->getRepository(Tupper::class);
            $tuppersAssigned = $tupperRepository->findByCategory($category->getSlug())->count();

            if($tuppersAssigned > 0){
                throw new \Exception("No puedes eliminar esta categoría, existe/n {$tuppersAssigned} tupper/s que la estan utilizando...");
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($category);
            $entityManager->flush();

            $resp = [
                'code'      =>  Response::HTTP_OK,
                'message'   =>  "Ingrediente eliminado correctamente."
            ];

        }catch(\Exception $e){
            $resp = [
                'code'      =>  Response::HTTP_FORBIDDEN,
                'message'   =>  $e->getMessage()
            ];
        }

        return new JsonResponse(['data' => $resp['message']], $resp['code']);
    }

    /**
     * @param int $currentPage
     * @return Response
     */
    public function ingredients(int $currentPage = 1)
    {
        /** @var IngredientRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Ingredient::class);

        $ingredientsPaginator = $repository->getAllIngredients($currentPage, $this->limit, $this->group, $this->search);
        $ingredients = $ingredientsPaginator->getQuery()->getResult();

        $totalItems = $ingredientsPaginator->count();
        $maxPages = ceil($totalItems / $this->limit);

        return $this->render('admin/ingredients/index.html.twig', [
            'data' => $ingredients,
            'maxPages'  => $maxPages,
            'thisPage'  => $currentPage,
            'filter_vars' => [
                'group'      =>  $this->group,
                'search'      =>  $this->search,
            ],
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createIngredient(Request $request){
        $ingredient = new Ingredient();

        //Create form
        $form = $this->createForm(IngredientType::class, $ingredient, [
            'action' => $this->generateUrl('admin_ingredients_create'),
        ]);

        //get form submit
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                //check if slug is unique
                /** @var Ingredient $found */
                $found = $this->getDoctrine()->getRepository(Ingredient::class)->findOneBy(['slug' => $ingredient->getSlug()]);
                if(null !== $found && $found->getId() !== $ingredient->getId()){
                    $slug = $ingredient->getSlug().uniqid();
                    $ingredient->setSlug($slug);
                }

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($ingredient);
                $entityManager->flush();

                return $this->redirectToRoute('admin_ingredients');

            }catch(DBALException $e){
                if(1062 == $e->getErrorCode()){
                    $form->addError(new FormError('Ha habido un problema al guadar el ingrediente en Base de datos.'));
                }

            }catch (\Exception $e){
                $form->addError(new FormError($e->getMessage()));
            }
        }

        return $this->render('admin/ingredients/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param int $ingredientid
     * @param EntityManagerInterface $entityManager
     * @return RedirectResponse|Response
     */
    public function editIngredient(Request $request, int $ingredientid, EntityManagerInterface $entityManager){

        /** @var Ingredient $ingredient */
        $ingredient = $entityManager->getRepository(Ingredient::class)->find($ingredientid);

        //ingredient does not exist
        if(null == $ingredient){
            throw $this->createNotFoundException('El ingrediente que quieres editar no existe.');
        }

        //Create form
        $form = $this->createForm(IngredientType::class, $ingredient, [
            'action' => $this->generateUrl('admin_ingredients_edit', ['ingredientid' => $ingredientid]),
        ]);

        //get form submit
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {

                //check if slug is unique
                /** @var Ingredient $found */
                $found = $this->getDoctrine()->getRepository(Ingredient::class)->findOneBy(['slug' => $ingredient->getSlug()]);
                if(null !== $found && $found->getId() !== $ingredient->getId()){
                    $slug = $ingredient->getSlug().uniqid();
                    $ingredient->setSlug($slug);
                }

                $entityManager->flush();

                return $this->redirectToRoute('admin_ingredients');

            }catch(DBALException $e){
                if(1062 == $e->getErrorCode()){
                    $form->addError(new FormError('Ha habido un problema al guadar el ingrediente en Base de datos.'));
                }

            }catch (\Exception $e){
                $form->addError(new FormError($e->getMessage()));
            }
        }


        return $this->render('admin/ingredients/create.html.twig', [
            'form' => $form->createView(),
            'edit_form' => true,
        ]);
    }

    public function deleteIngredient(Request $request, $ingredientid){
        try{
            /** @var Ingredient $ingredient */
            $ingredient = $this->getDoctrine()->getRepository(Ingredient::class)
                ->find($ingredientid);

            $tuppersAssigned = count($ingredient->getTuppers());
            if($tuppersAssigned > 0){
                throw new \Exception("No puedes eliminar este ingrediente, existe/n {$tuppersAssigned} tupper/s que lo estan utilizando...");
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ingredient);
            $entityManager->flush();

            $resp = [
                'code'      =>  Response::HTTP_OK,
                'message'   =>  "Ingrediente eliminado correctamente."
            ];

        }catch(\Exception $e){
            $resp = [
                'code'      =>  Response::HTTP_FORBIDDEN,
                'message'   =>  $e->getMessage()
            ];
        }

        return new JsonResponse(['data' => $resp['message']], $resp['code']);
    }
}
