<?php

namespace App\DataFixtures;

use App\Utils\UserState;
use DateTime;
use App\Entity\Category;
use App\Entity\Ingredient;
use App\Entity\Rating;
use App\Entity\Tag;
use App\Entity\Tupper;
use App\Entity\TuppersIngredients;
use App\Entity\User;
use App\Utils\Difficulty;
use App\Utils\Duration;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Create 2 users
        $users = [];
        $user = new User();
        $user
            ->setEnabled(true)
            ->setName('Laura')
            ->setSurname('Fernández')
            ->setEmail('lfernandez6+test1@gmail.com');
        $manager->persist($user);
        $user
            ->setPassword('$2y$13$AQ.tP2hC5RanYwwdvSYNZ.qpKXnCU0qV0uMG0xaW8twqWZ6PWVxjG') //password: asdf
            ->setRoles(['ROLE_DEFAULT_USER'])
            ->setRegisterDate(new DateTime());
        $manager->persist($user);
        $users[] = $user;

        $user = new User();
        $user
            ->setEnabled(true)
            ->setName('Pepa')
            ->setSurname('Pérez')
            ->setEmail('lfernandez6+test2@gmail.com');
        $manager->persist($user);
        $user
            ->setPassword('$2y$13$AQ.tP2hC5RanYwwdvSYNZ.qpKXnCU0qV0uMG0xaW8twqWZ6PWVxjG') //password: asdf
            ->setRoles(['ROLE_DEFAULT_USER'])
            ->setRegisterDate(new DateTime());
        $manager->persist($user);
        $users[] = $user;

        // Create 10 categories
        $categories = [];
        for ($i = 0; $i < 10; $i++) {
            $category = new Category();
            $category
                ->setSlug('category'.$i)
                ->setName('Category '.$i);

            $categories[] = $category;
            $manager->persist($category);
        }

        // Create 10 ingredients
        $ingredients = [];
        for ($i = 0; $i < 10; $i++) {
            $ingredient = new Ingredient();
            $ingredient
                ->setSlug('ingredient'.$i)
                ->setName('Ingredient '.$i);

            $ingredients[] = $ingredient;
            $manager->persist($ingredient);
        }

        // Create 10 tags
        $tags = [];
        for ($i = 0; $i < 10; $i++) {
            $tag = new Tag();
            $tag
                ->setSlug('tag'.$i);

            $tags[] = $tag;
            $manager->persist($tag);
        }

        // tupper
        $tuppers = [];
        for ($i = 0; $i < 30; $i++) {
            $tupper = new Tupper();
            $tupper
                ->setSlug('tupper'.$i)
                ->setTitle('Tupper '.$i)
                ->setCategory($categories[rand(0,9)])
                ->setDifficulty(Difficulty::LOW)
                ->setDuration(Duration::DURATION_30)
                ->setCommensals(rand(1,4))
                ->setSteps('steps tupper '.$i)
                ->setImage('https://dummyimage.com/600x400/b5afb5/fff.png')
                ->setCreationDate(new DateTime())
                ->setUser($users[rand(0,1)])
                ->setVisits(rand(0,20))
                ->addTag($tags[0])
                ->addTag($tags[1])
                ->addTag($tags[2]);

            $t_ing = new TuppersIngredients();
            $t_ing
                ->setTupper($tupper)
                ->setIngredient($ingredients[rand(0,9)])
                ->setQuantity(rand(10, 500). " gr");
            $t_ing2 = new TuppersIngredients();
            $t_ing2
                ->setTupper($tupper)
                ->setIngredient($ingredients[rand(0,9)])
                ->setQuantity(rand(10, 500). " gr");
            $t_ing3 = new TuppersIngredients();
            $t_ing3
                ->setTupper($tupper)
                ->setIngredient($ingredients[rand(0,9)])
                ->setQuantity(rand(10, 500). " gr");

            $tupper->addIngredient($t_ing);
            $tupper->addIngredient($t_ing2);
            $tupper->addIngredient($t_ing3);

            $tuppers[] = $tupper;
            $manager->persist($tupper);
        }

        // rating
        for ($i = 0; $i < 10; $i++) {
            $rating = new Rating();
            $rating
                ->setUser($users[rand(0,1)])
                ->setTupper($tuppers[rand(0,9)])
                ->setRate(rand(0,5));
            $manager->persist($rating);
        }



        $manager->flush();
    }
}
