"use strict";
(function ($) {
    var FormValidate = {
        init: function () {
            console.log('FormValidate loaded.');
            this.initItems();
        },
        initItems: function(){
            let self = this;

            $('form.form_createtupper').submit(function (e) {
                return self.submitForm();
            });
        },
        submitForm: function () {

            // hide errors
            Utils.hideError();
            $('form').find('.error').removeClass('error');

            // get form fields
            let params = {};
            params.title = $('#tupper_title').val();
            params.category = $('#tupper_category').val();
            params.difficulty = $('#tupper_difficulty').val();
            params.duration = $('#tupper_duration').val();
            params.commensals = $('#tupper_commensals').val();
            params.steps = $('#tupper_steps').val();
            params.isEdit = $('#is_edit').val();
            params.image = $('#tupper_image').val();

            // validate fields
            let validation = this.validateForm(params);

            // if form has errors
            if(validation != ""){
                let errors = '';
                $.each(validation, function(key,value){
                    $('#'+value[0]).addClass('error');
                    errors += value[1] + '<br>';
                });
                Utils.showError(errors);

                return false;
            }

            //success form data
            Utils.hideError();
            Utils.showLoader();

            return true;
        },
        validateForm: function (params) {
            let result = [];

            if(params.title === ''){
                result.push(['tupper_title', 'El título del tupper no puede estar en blanco.']);
            }
            if(params.category === ''){
                result.push(['tupper_category', 'No has indicado la categoría de la receta.']);
            }
            if(params.difficulty === ''){
                result.push(['tupper_difficulty', 'No has indicado la dificultad de la receta.']);
            }
            if(params.duration === ''){
                result.push(['tupper_duration', 'No has indicado la duración de la receta.']);
            }

            if (isNaN(parseInt(params.commensals)) || parseInt(params.commensals) <= 0){
                result.push(['tupper_commensals', 'La receta debe ser para almenos debe ser para 1 comensal.']);
            }
            if(params.steps === ''){
                result.push(['tupper_steps', 'Debes describir los pasos necesarios para realizar la receta.']);
            }
            if(params.isEdit === undefined && params.image === ''){
                result.push(['tupper_image', 'Debes seleccionar una imagen.']);
            }

            return result;
        },

    };

    FormValidate.init();
})(jQuery);
