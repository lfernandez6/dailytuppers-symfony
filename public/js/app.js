"use strict";

let numTags = $('input[name="tupper[tags][][slug]"]').length;
let numIngredients = ($('input[name^="tupper[ingredients]"]').length)/2;
let searchIngredients = [];

(function ($) {
    var App = {
        init: function () {
            console.log('App loaded.');
            this.initFilters();
            this.initItems();

            this.reloadIngredients();
            this.loadAutocomplete();
        },

        /**
         * FILTERS
         */
        initFilters: function(){
            let self = this;
            // Filter Sort
            $('#select_orderby').change(function () {
                self.filter();
            });
            // Search keyword box
            $('#filters_search').click(function () {
                self.filter();
            });
            // Filter selects
            $('.select-filters').change(function () {
                self.filter();
            });
            // Filter to paginator
            $('.page-link').click(function (e) {
                e.preventDefault();
                self.filter($(this).attr('href'), true);
            });
            // Do search by ingredient
            $('.do-search-by-ingredient').click(function () {
                if(searchIngredients.length > 0){
                    self.filter();
                }
            });
        },
        initItems: function(){
            let self = this;

            // Delete TAGS
            $('.delete-tag').click(function () {
                self.deleteTag($(this));
            });

             // Delete INGREDIENTS
            $('.delete-ingredient').click(function () {
                self.deleteIngredient($(this));
            });

            // Delete SEARCH - INGREDIENTS
            $('.delete-search-ingredient').click(function () {
                self.deleteSearchIngredient($(this));
            });

            // Delete Tupper
            $('.delete-tupper').click(function (){
                self.deleteTupper($(this))
            });

            // Add Tag
            $('.add-tag').click(function (){
                self.addTag();
            });

            // Add Ingredient
            $('.add-ingredient').click(function () {
                self.addIngredient();
            });

            // Add SearchIngredient
            $('.add-search-ingredient').click(function () {
                self.addSearchIngredient();
            });

            // Rating
            $('.trigger_rate .fa-star').click(function (){
                self.starRating($(this));
            });

            // modal delete button
            $('.btn-delete').click(function (){
                self.modalDeleteButton($(this));
            });

            // close alert
            $(".alert .close").click(function () {
                self.closeAlert($(this));
            });
        },
        /**
         * Load ingredients (add them to array searchIngredients)
         * Only if exist ingredients into GET[ingredients] param
         */
        reloadIngredients: function(){
            if($('.add-search-ingredient').length > 0){
                let items = $('span.badge.ingredient');
                if (items.length > 0){
                    $.each( items, function( key, value ) {
                        let id = $(value).children('a').data('ingredientid');
                        searchIngredients.push(id);
                    });
                }
            }
        },
        loadAutocomplete: function(){
            if($('#ingredient').length > 0){
                //select2
                var options = {
                    url_list: $('#url-list').attr('href'),
                    url_get: $('#url-get').attr('href'),
                    otherOptions: {
                        minimumInputLength: 3,
                        formatNoMatches: 'No se han encontrado ingredientes.',
                        formatSearching: 'Buscando ingredientes...',
                        formatInputTooShort: 'Escribe almenos 3 letras'
                    }
                };
                $('#ingredient').autocompleter(options);
            }
        },
        addTag: function(){
            let self = this;
            let $form = $('.form_createtupper');
            let tag = $('input[name="tag"]').val();
            let box = $('.box-tag');

            //if emtpy tag, do nothing
            if(tag == '') {
                return false;
            }

            //slugify
            tag = Utils.slugify(tag);

            //get next id
            numTags++;
            let tagId = numTags;

            box.append('<span class="badge badge-pill badge-secondary align-middle x12 mr-1 mb-1 tag">' +
                tag +
                ' <a href="javascript:void(0)" class="text-white delete-tag" data-tagId="'+ tagId +'" title="Eliminar etiqueta"><i class="material-icons x12">close</i></a>' +
                '</span>');

            //unbind and bind clic event
            $('.delete-tag').unbind('click');
            $('.delete-tag').bind('click', function () {
                self.deleteTag($(this));
            });

            //add input hidden to store Tag object
            $form.append('<input type="hidden" name="tupper[tags][][slug]" id="tupper_tag_'+ tagId +'" value="'+ tag +'" >');

            //empty input value
            $('input[name="tag"]').val('');
        },
        addIngredient: function(){
            let self = this;

            //hide errors
            Utils.hideError();
            $('input[name="quantity"]').removeClass('error');

            let $form = $('.form_createtupper');
            let ingredient = $('#select2-chosen-1').html();
            //ingredient_id from DB
            let ing_id_db = $('input[name="ingredient"]').val();
            let quantity = $('input[name="quantity"]').val();
            let box = $('.box-ingredient');

            //if emtpy ingredient, do nothing
            if(ingredient == '' || ingredient == "Busca otro ingrediente" || ingredient == "Ingrediente (Ej. azúcar)") {
                return false;
            }

            //if empty quantity, show error and do nothing
            if(quantity == ''){
                $('input[name="quantity"]').addClass('error');
                Utils.showError("Debes indicar la cantidad");
                return false;
            }

            let union = (quantity.length > 1) ? " de " : " ";
            let displayName = quantity + union + ingredient;

            //get next id
            let ingredientId = numIngredients;

            box.append('<span class="badge badge-pill badge-secondary align-middle x12 mr-1 mb-1 ingredient">' +
                displayName +
                ' <a href="javascript:void(0)" class="text-white delete-ingredient" data-ingredientId="'+ ingredientId +'" title="Eliminar ingrediente"><i class="material-icons x12">close</i></a>' +
                '</span>');

            //unbind and bind clic event
            $('.delete-ingredient').unbind('click');
            $('.delete-ingredient').bind('click', function () {
                self.deleteIngredient($(this));
            });

            //add input hidden to store Tag object
            $form.append('<input type="hidden" name="tupper[ingredients]['+ ingredientId +'][ingredient]" id="tupper_ingredients_'+ ingredientId +'_ingredient" value="'+ ing_id_db +'" >');
            $form.append('<input type="hidden" name="tupper[ingredients]['+ ingredientId +'][quantity]" id="tupper_ingredients_'+ ingredientId +'_quantity" value="'+ quantity +'" >');

            //increment index
            numIngredients++;

            //empty input values and clear errors
            $('input[name="ingredient"]').val('');
            $('#select2-chosen-1').html('Busca otro ingrediente');
            $('input[name="quantity"]').val('');
            $('input[name="quantity"]').removeClass('error');
            Utils.hideError();
        },
        addSearchIngredient: function(){
            let self = this;
            let ingredient = $('#select2-chosen-1').html();
            //ingredient_id from DB
            let ing_id_db = $('input[name="ingredient"]').val();
            let box = $('.box-ingredient');

            //if emtpy ingredient, do nothing
            if(ingredient == '' || ingredient == "Busca otro ingrediente" || ingredient == "Ingrediente (Ej. azúcar)") {
                return false;
            }

            box.append('<span class="badge badge-pill badge-secondary align-middle x12 mr-1 mb-1 ingredient">' +
                ingredient +
                ' <a href="javascript:void(0)" class="text-white delete-search-ingredient" data-ingredientId="'+ ing_id_db +'" title="Eliminar ingrediente"><i class="material-icons x12">close</i></a>' +
                '</span>');

            //unbind and bind clic event
            $('.delete-search-ingredient').unbind('click');
            $('.delete-search-ingredient').bind('click', function () {
                self.deleteSearchIngredient($(this));
            });

            //add ingredient to array search
            searchIngredients.push(ing_id_db);

            //empty input values and clear errors
            $('input[name="ingredient"]').val('');
            $('#select2-chosen-1').html('Busca otro ingrediente');
        },
        deleteTag: function($obj){
            let tagId = $obj.data('tagid');
            let tag = $obj.parent('span.badge');
            tag.remove();
            $('input#tupper_tag_'+tagId).val('');
        },
        deleteIngredient: function($obj){
            let ingredientId = $obj.data('ingredientid');
            let ingredient = $obj.parent('span.badge');
            ingredient.remove();
            $('input#tupper_ingredients_'+ ingredientId +'_ingredient').val('');
            $('input#tupper_ingredients_'+ ingredientId +'_quantity').val('');
        },
        deleteSearchIngredient: function($obj){
            let ingredientId = $obj.data('ingredientid');
            let ingredient = $obj.parent('span.badge');
            ingredient.remove();

            //quitar ingredient del array
            searchIngredients = Utils.arrayRemove(searchIngredients, ingredientId);

        },
        deleteTupper: function($obj){
            let tupperId = $obj.data('tupperid');
            let isDetail = ($obj.data('isdetail') != undefined) ? $obj.data('isdetail') : false;

            $('#deleteTupperModal').find('.btn-delete').data('tupperid', tupperId);
            $('#deleteTupperModal').find('.btn-delete').data('isdetail', isDetail);
            $('#deleteTupperModal').modal('show');
        },
        starRating: function($objStar){
            Utils.showLoader();
            let rate = $objStar.data('rate');
            let tupperId = $objStar.parent().data('tupperid');

            fetch(`/tupper/${tupperId}/votar/${rate}`, {
                method: 'PUT',
            }).then(function (response) {
                switch (response.status){
                    case 200:
                        response.json().then(function(json){
                            let $stars = $objStar.parent('.rating').find('.fa-star');
                            $stars.removeClass('checked');

                            $stars.each(function(){
                                let currentValue = $(this).data('rate');
                                if (currentValue > rate) return;
                                $(this).addClass('checked');
                            });
                            Utils.hideLoader();
                        }).catch(function(error) {
                            Utils.hideLoader();
                        });
                        break;
                    case 401:
                        Utils.hideLoader();
                        //show modal
                        $('#notLoggedModal').modal('show');
                        break;
                }
                Utils.hideLoader();
            }).catch(function(error) {
                console.log('Hubo un problema al votar el tupper:' + error.message);
            });
        },
        filter: function(href = '', isPaginated = false) {
            let params = {};

            let sort = $('#select_orderby option:selected').val();
            let sort_url = $('#select_orderby option:selected').data('sort');

            let search = $('input[name="input-search"]').val();
            let category = $('#select_filter_categoria').val();
            let difficulty = $('#select_filter_dificultad').val();
            let duration = $('#select_filter_duracion').val();

            //admin
            let group = $('#select_filter_group').val();

            if('' != sort_url && !isPaginated){
                href = sort_url;
            }else if('' != sort_url && isPaginated){
                //do nothing
            }else{
                params.ordenar = sort;
            }

            if('' != search && undefined != search){
                params.buscar = search;
            }
            if('' != category && undefined != category){
                params.categoria = category;
            }
            if('' != difficulty && undefined != difficulty){
                params.dificultad = difficulty;
            }
            if('' != duration && undefined != duration){
                params.duracion = duration;
            }
            if(searchIngredients.length > 0){
                params.ingredients = searchIngredients.join();
            }

            //Admin
            if('' != group && undefined != group){
                params.grupo = group;
            }

            if((Object.keys(params).length == 0) && '' == href){
                return false;
            }

            Utils.showLoader();
            let destination_url = ('' == href) ? $('input[name="destination_url"]').val() : href;
            let searchUrl = destination_url;

            if(Object.keys(params).length > 0){
                searchUrl += '?'+ jQuery.param( params );
            }

            document.location.href = searchUrl;
        },
        /** Close ALERTS */
        closeAlert: function($obj){
            $obj.parent(".alert").slideUp(400);
        },
        /** Delete button modal */
        modalDeleteButton: function($obj){

            let tupperId = $obj.data('tupperid');
            let isDetail = $obj.data('isdetail');
            let actionUrl = `/mi-cuenta/eliminar-tupper/${tupperId}`;

            Utils.showLoader();
            fetch(actionUrl, {
                method: 'DELETE'

            }).then(function (response) {
                switch (response.status){
                    case 200:
                        response.json().then(function(json){
                            console.log(json);
                            Utils.hideLoader();
                            if(isDetail == true){
                                window.location.href = "/mi-cuenta/mis-tuppers";
                            }else{
                                window.location.reload()
                            }
                        }).catch(function(error) {
                            Utils.hideLoader();
                        });
                        break;
                    case 401:
                        Utils.hideLoader();
                        //show modal
                        $('#notLoggedModal').modal('show');
                        break;
                }
                Utils.hideLoader();

            }).catch(function(error) {
                console.log('Hubo un problema al eliminar el tupper:' + error.message);
            });

            $('#deleteTupperModal').modal('toggle');
        },
    };

    App.init();
})(jQuery);
