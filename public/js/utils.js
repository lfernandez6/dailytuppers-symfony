let Utils;

"use strict";
(function ($) {
    Utils = {
        init: function () {
            console.log('Utils loaded.');
            this.initElements();
        },
        initElements: function(){
            /**
             * show loader when click button
             */
            $('.btn-loader').click(this.showLoader);

            /**
             * On submit form, show loader
             */
            $('.useFormLoader').submit(this.showLoader);
        },
        showSuccess: function(message) {
            $('.alert-success span').html(message);
            $(".alert-success").slideDown(400);
            // $('.alert-success').alert();
        },
        hideSuccess: function() {
            $(".alert-success").slideUp(400);
        },
        showError: function(message){
            $('.alert-danger div.form-error').html(message);
            $('.alert-danger').css('display', 'block');
            $(".alert-danger").slideDown(400);
        },
        hideError: function() {
            $('.alert-danger').css('display', 'none');
            $(".alert-danger").slideUp(400);
        },
        showLoader: function() {
            $('.preloader').fadeIn(300);
        },
        hideLoader: function() {
            $('.preloader').fadeOut(300);
        },
        slugify: function(str) {
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
            var to   = "aaaaeeeeiiiioooouuuunc------";
            for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            return str;
        },
        arrayRemove: function(arr, value) {
            return arr.filter(function(ele){
                return ele != value;
            });
        }
    };

    Utils.init();
})(jQuery);
