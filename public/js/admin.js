"use strict";
(function ($) {
    var Admin = {
        init: function () {
            this.initItems();
        },
        initItems: function(){
            let self = this;

            // Ingredients - filter group
            $('select[name="select_filter_group"]').change(function () {
                // self.filterIngredients($(this));
                self.filter();
            });
            $('.admin_search').click(function (){
                self.filter();
            });


            // Delete Item
            $('.delete-item').click(function (){
                self.deleteItem($(this))
            });

            // modal delete button
            $('.btn-modal-delete').click(function (){
                self.modalDeleteButton($(this));
            });

            // Block/Unblock user
            $('.block-user').click(function (){
                self.blockUser($(this));
            });

            // Publish/Unpublish tupper
            $('.publish-tupper').click(function (){
                self.publishTupper($(this));
            });
        },
        filterIngredients: function ($obj) {
            let url = $obj.data('url');
            let group = $('#select_filter_group').val();

            Utils.showLoader();
            document.location.href = url + '?grupo=' + group;
        },
        filter: function(href = '') {
            let params = {};

            let search = $('input[name="input-search"]').val();
            let group = $('#select_filter_group').val();

            if('' != search && undefined != search){
                params.buscar = search;
            }
            if('' != group && undefined != group){
                params.grupo = group;
            }

            if((Object.keys(params).length == 0) && '' == href){
                return false;
            }

            Utils.showLoader();
            let destination_url = ('' == href) ? $('input[name="destination_url"]').val() : href;
            let searchUrl = destination_url;

            if(Object.keys(params).length > 0){
                searchUrl += '?'+ jQuery.param( params );
            }

            document.location.href = searchUrl;
        },
        deleteItem: function($obj){
            let itemId = $obj.data('itemid');
            let type = $obj.data('type');

            $('#deleteItemModal').find('.btn-modal-delete').data('itemid', itemId);
            $('#deleteItemModal').find('.btn-modal-delete').data('type', type);
            $('#deleteItemModal').modal('show');
        },

        /** Delete button modal */
        modalDeleteButton: function($obj){

            let itemId = $obj.data('itemid');
            let type = $obj.data('type');
            let actionUrl = this.getDeleteUrl(type, itemId);

            console.log('delete item ['+type+']: '+itemId);

            Utils.showLoader();
            fetch(actionUrl, {
                method: 'DELETE'

            }).then(function (response) {
                switch (response.status){
                    case 200:
                        response.json().then(function(json){
                            console.log(json);
                            window.location.reload()
                        }).catch(function(error) {
                            Utils.hideLoader();
                        });
                        break;

                    default:
                        response.json().then(function(json){
                            console.log(json);
                            $('#deleteItemModal').modal('toggle');
                            Utils.hideLoader();
                            Utils.showError(json.data);
                            return false;
                        }).catch(function(error) {
                            console.log(error);
                            $('#deleteItemModal').modal('toggle');
                            Utils.hideLoader();
                        });

                        break;
                }
                Utils.hideLoader();

            }).catch(function(error) {
                console.log('Hubo un problema al eliminar el elemento:' + error.message);
            });

            $('#deleteTupperModal').modal('toggle');
        },

        blockUser: function($obj){

            let userId = $obj.data('userid');
            let banned = $obj.data('banned');
            let actionUrl = `/admin/usuarios/bloquear/${userId}/${banned}`;

            console.log('block ['+banned+'] user: '+userId);

            Utils.showLoader();
            fetch(actionUrl, {
                method: 'PUT'

            }).then(function (response) {
                switch (response.status){
                    case 200:
                        response.json().then(function(json){
                            console.log(json);
                            window.location.reload()
                        }).catch(function(error) {
                            console.log(error);
                            Utils.hideLoader();
                        });
                        break;

                    default:
                        response.json().then(function(json){
                            console.log(json);
                            Utils.hideLoader();
                            Utils.showError(json.data);
                            return false;
                        }).catch(function(error) {
                            console.log(error);
                            Utils.hideLoader();
                        });

                        break;
                }
                Utils.hideLoader();

            }).catch(function(error) {
                console.log('Hubo un problema al eliminar el elemento:' + error.message);
            });
        },

        publishTupper: function($obj){

            let tupperId = $obj.data('tupperid');
            let published = $obj.data('published');
            let actionUrl = `/admin/tuppers/publish/${tupperId}/${published}`;

            console.log('publish ['+published+'] tupperid: '+tupperId);

            Utils.showLoader();
            fetch(actionUrl, {
                method: 'PUT'

            }).then(function (response) {
                switch (response.status){
                    case 200:
                        response.json().then(function(json){
                            console.log(json);
                            window.location.reload()
                        }).catch(function(error) {
                            console.log(error);
                            Utils.hideLoader();
                        });
                        break;

                    default:
                        response.json().then(function(json){
                            console.log(json);
                            Utils.hideLoader();
                            Utils.showError(json.data);
                            return false;
                        }).catch(function(error) {
                            console.log(error);
                            Utils.hideLoader();
                        });

                        break;
                }
                Utils.hideLoader();

            }).catch(function(error) {
                console.log('Hubo un problema al cambiar el estado del tupper:' + error.message);
            });
        },
        getDeleteUrl: function (type, itemId) {
            switch (type){
                case 'ingredient':
                    return `/admin/ingredientes/eliminar/${itemId}`;
                    break;
                case 'category':
                    return `/admin/categorias/eliminar/${itemId}`;
                    break;
                case 'user':
                    return `/admin/usuarios/eliminar/${itemId}`;
                    break;
            }
        }
    };

    Admin.init();
})(jQuery);
