'use strict';

const sass = require('node-sass');

module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                process: function(src, filepath) {
                    return '// Source: ' + filepath + '\n' +
                        src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
                }
            },
            dist: {
                src: [
                    'public/js/vendor/jquery/jquery.js',
                    'public/js/vendor/popperjs/popper.js',
                    'public/js/vendor/bootstrap/bootstrap.js',
                ],
                dest: 'public/js/vendor.js'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src: [
                    'public/js/vendor.js'
                ],
                dest: 'public/js/vendor.min.js'
            }
        },
        sass: {
            options: {
                implementation: sass
            },
            compile: {
                files: {
                    'public/css/app.css': 'public/scss/app.scss'
                }
            }
        },
        postcss: {
            options: {
                map: false, // inline sourcemaps
                processors: [
                    require('autoprefixer')({browsers: 'last 2 versions'}), // add vendor prefixes
                    require('cssnano')() // minify the result
                ]
            },
            dist: {
                src: 'public/css/*.css'
            }
        },
        watch: {
            sass: {
                files: [
                    "public/scss/**/*"
                ],
                tasks: [
                    "sass",
                    "postcss"
                ]
            },
            js: {
                files: [
                    'public/js/vendor/**/*.js',
                    "gruntfile.js"
                ],
                tasks: [
                    "concat",
                    "uglify"
                ]
            }

        },
        //JUST FOR VENDORS
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        src: ['node_modules/jquery/dist/jquery.js'],
                        dest: 'public/js/vendor/jquery/',
                        flatten: true,
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        src: ['node_modules/bootstrap/dist/js/bootstrap.js'],
                        dest: 'public/js/vendor/bootstrap/',
                        flatten: true,
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        src: ['node_modules/popper.js/dist/umd/popper.js'],
                        dest: 'public/js/vendor/popperjs/',
                        flatten: true,
                        filter: 'isFile'
                    }
                ]
            }
        }
    });

    // Loads
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-babel');

    // Default task(s).
    grunt.registerTask('default', ['watch']);
};

