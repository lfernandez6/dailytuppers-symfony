# Daily Tuppers (Symfony project) 

### Requirements to install and run Symfony

1. PHP version: **7.1.3** or higher.

2. To have **composer** installed.

3. PHP extensions: (all of them are installed and enabled by default in PHP 7+)
    - Ctype
    - iconv
    - JSON
    - PCRE
    - Session
    - SimpleXML
    - Tokenizer
    
4. Writable directories: (must be writable by the web server)
    - The project's cache directory (**var/cache/** by default, but the app can override the cache dir)
    - The project's log directory (**var/log/** by default, but the app can override the logs dir) 
       

### First installation

1. Clone de project with GIT:

        git clone git@gitlab.com:lfernandez6/dailytuppers-symfony.git
 
2. Install dependencies with composer:
 
         cd dailytuppers-symfony
         composer install
         
3. Configure .env file (copy .env.dist and rename it to .env). Then configure the database connection.

        DATABASE_URL=mysql://{db_user}:{db_password}@127.0.0.1:3306/{db_name}

4. Create the database with the command:

        php bin/console doctrine:database:create

5. Create tables with commands:

        php bin/console doctrine:schema:update --dump-sql
        php bin/console doctrine:schema:update --force
        
6. Load fake data with the command:

        php bin/console doctrine:fixtures:load

### Server configuration

**Option A**. Run de server with the command:

    php bin/console server:run
    
Server listening on http://127.0.0.1:8000

        
**Option B**. configure the Apache server:
        
- Add rewrite rules (installs a .htaccess file in the public/ directory that contains the rewrite rules):
        
        composer require symfony/apache-pack
            
- Apache minimum configuration:

        <VirtualHost *:80>
            ServerName domain.tld
            ServerAlias www.domain.tld
        
            DocumentRoot /var/www/project/public
            <Directory /var/www/project/public>
                # enable the .htaccess rewrites
                AllowOverride All
                Require all granted
            </Directory>
        
            # uncomment the following lines if you install assets as symlinks
            # or run into problems when compiling LESS/Sass/CoffeeScript assets
            # <Directory /var/www/project>
            #     Options FollowSymlinks
            # </Directory>
        
            ErrorLog /var/log/apache2/project_error.log
            CustomLog /var/log/apache2/project_access.log combined
        </VirtualHost>
        
More information: https://symfony.com/doc/current/setup/web_server_configuration.html


#### Utilities:
 
- Information about the app:
        
        php bin/console about
        
- Get a list of all routes on the application:

        php bin/console debug:router
        
- Get a list of all events on the application:

        php bin/console debug:event-dispatcher
        
- Generate new controllers:

        php bin/console make:controller

- Doctrine: create database

        php bin/console doctrine:database:create
        
- Doctrine: after create the entities, validate the mappings

        php bin/console doctrine:schema:validate

- Doctrine: generate dump from Entity annotations

        php bin/console doctrine:schema:update --dump-sql

- Doctrine: import dump into database

        php bin/console doctrine:schema:update --force

- Symfony Fixtures Bundle: Load fake data (https://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html)

        php bin/console doctrine:fixtures:load
        
### FOS User Bundle
- Assing a Role to a user (change {user_email} by the user email):

        php bin/console fos:user:promote {user_email} ROLE_SUPER_ADMIN
        
    Role for default users:
    
        php bin/console fos:user:promote {user_email} ROLE_DEFAULT_USER
        
#### !!! Fix possible Error MySql
Error in GROUP BY queries
> **Error Code: 1055**. Expression #1 of SELECT list is not in GROUP BY clause and contains nonaggregated column [...] which is not functionally dependent on columns in GROUP BY clause; this is incompatible with **sql_mode=only_full_group_by**
        
        mysql> set global sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
        mysql> set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';

### Execute Unit/Functional Tests

```
bin/phpunit src/Tests
```

- Check Site urls:
        
        bin/phpunit src/Tests/Controller/SiteUrlsTest
        
- Check Utils:
        
        bin/phpunit src/Tests/Utils/GeneralTest
        
- Check Repositories:
        
        bin/phpunit src/Tests/Repository/RepositoryTest
        
